package com.ruoyi.system.api.taskapi;


import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.system.api.factory.RemoteFileFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@FeignClient(contextId = "taskOrderApi", value = "shipper")
public interface TaskOrderApi {
     @PutMapping("/task/upstate/{id}")
     Long upstate(@PathVariable("id") Long id);

     @PutMapping("/task/upstateComplete/{id}")
     Long upstateComplete(@PathVariable("id") Long id);


     @PutMapping("/task/upstateTransport/{id}")
     Long upstateTransport(@PathVariable("id") Long id);

     @GetMapping("/task/selectTaskStatus/{id}")
      AjaxResult selectTaskStatus(@PathVariable("id")Long id);

}
