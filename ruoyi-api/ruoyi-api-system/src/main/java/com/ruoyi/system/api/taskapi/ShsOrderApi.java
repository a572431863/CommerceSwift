package com.ruoyi.system.api.taskapi;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@FeignClient(contextId = "ShsOrderApi", value = "shipper")
public interface ShsOrderApi {

    /**
     * 修改货主装货状态
     */
    @PutMapping("/order/upShsStateShipment/{id}")
     Long upShsStateShipment(@PathVariable("id") Long id);

    /**
     * 修改货主运输状态
     */
    @PutMapping("/order/upShsStateTransport/{id}")
     Long upShsStateTransport(@PathVariable("id") Long id);
    /**
     * 修改货主完成订单状态
     */
    @PutMapping("/order/upShsStateComplete/{id}")
     Long upShsStateComplete(@PathVariable("id") Long id);

    /**
     * 修改车主装货状态
     */
    @PutMapping("/order/upstate/{id}")
     Long upCarStateShipment(@PathVariable("id") Long id);

    /**
     * 修改车主运输状态
     */
    @PutMapping("/order/upCarStateTransport/{id}")
     Long upCarStateTransport(@PathVariable("id") Long id);

    /**
     * 修改车主完成订单状态
     */
    @PutMapping("/order/upCarStateComplete/{id}")
     Long upCarStateComplete(@PathVariable("id") Long id);

}
