package com.ruoyi.system.api.orderapi;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(contextId = "OrderApi", value = "car-order")
public interface OrderApi {


    @PostMapping("/order/updateByState")
    Integer updateByState(Long orderId,String state);
}
