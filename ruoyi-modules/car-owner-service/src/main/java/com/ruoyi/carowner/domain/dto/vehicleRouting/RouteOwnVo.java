package com.ruoyi.carowner.domain.dto.vehicleRouting;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
/**
 * 车主个人所属的车辆路线
 */
public class RouteOwnVo extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 外键id
     */
    @Excel(name = "类型外键")
    private Long shipType;
    
    @Excel(name = "到达地")
    private String arrival;

    @Excel(name = "出发地")
    private String departure;

    @Excel(name = "运输类型：整车/零担")
    private String brief;
}
