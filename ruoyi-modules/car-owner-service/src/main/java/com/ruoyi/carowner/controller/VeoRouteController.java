package com.ruoyi.carowner.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteQueryDto;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteBasicVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.carowner.domain.VeoRoute;
import com.ruoyi.carowner.service.IVeoRouteService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车主路线Controller
 *
 * @author ruoyi
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/route")
public class VeoRouteController extends BaseController {
    @Autowired
    private IVeoRouteService veoRouteService;

    /**
     * 查询所有路线列表
     */
    @RequiresPermissions("carowner:route:list")
    @GetMapping("/list")
    public TableDataInfo list(@RequestBody RouteQueryDto routeQueryDto) {
        startPage();  // 分页
        List<RouteBasicVo> list = veoRouteService.showAll(routeQueryDto);
        return getDataTable(list);

    }
    //    public TableDataInfo list(VeoRoute veoRoute)
//    {
//        startPage();
//        List<VeoRoute> list = veoRouteService.selectVeoRouteList(veoRoute);
//        return getDataTable(list);
//    }


    /**
     * 通过登录id获取车主个人发布的路线信息
     */
    @RequiresPermissions("carowner:route:querys")
    @GetMapping("/querys")
    public AjaxResult selectRouteOwn() {
        return success(veoRouteService.selectRouteOwnVo());
    }

    /**
     * 导出车主路线列表
     */
    @RequiresPermissions("carowner:route:export")
    @Log(title = "车主路线", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, VeoRoute veoRoute) {
        List<VeoRoute> list = veoRouteService.selectVeoRouteList(veoRoute);
        ExcelUtil<VeoRoute> util = new ExcelUtil<VeoRoute>(VeoRoute.class);
        util.exportExcel(response, list, "车主路线数据");
    }

    /**
     * 获取车主路线详细信息
     */
    @RequiresPermissions("carowner:route:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(veoRouteService.selectVeoRouteById(id));
    }

    /**
     * 新增车主路线
     */
    @RequiresPermissions("carowner:route:add")
    @Log(title = "车主路线", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VeoRoute veoRoute) {
        return toAjax(veoRouteService.insertVeoRoute(veoRoute));
    }

    /**
     * 修改车主路线
     */
    @RequiresPermissions("carowner:route:edit")
    @Log(title = "车主路线", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody VeoRoute veoRoute) {
        return toAjax(veoRouteService.updateVeoRoute(veoRoute));
    }

    /**
     * 删除车主路线
     */
    @RequiresPermissions("carowner:route:remove")
    @Log(title = "车主路线", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(veoRouteService.deleteVeoRouteByIds(ids));
    }

    /**
     * 查询单条路线详情
     */
    @RequiresPermissions("carowner:route:queryById")
    @GetMapping("/detail/{routeId}")
    public AjaxResult findById(@PathVariable("routeId") Long routeId) {
        return success(veoRouteService.findById(routeId));
    }
}
