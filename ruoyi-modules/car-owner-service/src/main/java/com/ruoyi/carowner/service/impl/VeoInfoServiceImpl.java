package com.ruoyi.carowner.service.impl;

import com.ruoyi.carowner.domain.Car;
import com.ruoyi.carowner.domain.VeoInfo;
import com.ruoyi.carowner.domain.dto.vehicleManagement.VeoRegisterDto;
import com.ruoyi.carowner.mapper.CarMapper;
import com.ruoyi.carowner.mapper.VeoInfoMapper;
import com.ruoyi.carowner.service.IVeoInfoService;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.IdWorker;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.domain.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 车主个人信息Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-07
 */
@Service
public class VeoInfoServiceImpl implements IVeoInfoService
{
    @Autowired
    private VeoInfoMapper veoInfoMapper;
    @Autowired
    private CarMapper carMapper;
    @Autowired
    private RemoteUserService remoteUserService;




    /**
     * 注册车主
     * @param registerDto 车主注册信息
     * @return
     */
    @Override
    @Transactional
    public int insertVeoInfo(VeoRegisterDto registerDto) {
        Car car = new Car();
        car.setId(IdWorker.nextId());
        car.setPlateNum(registerDto.getPlateNum());
        car.setSizeId(registerDto.getSizeId());
        car.setTypeId(registerDto.getTypeId());
        car.setImg(registerDto.getImg());
        carMapper.insertCar(car);


        SysUser sysUser = new SysUser();
        sysUser.setUserId(IdWorker.nextId());
        sysUser.setPhonenumber(registerDto.getPhoneNumber());
        sysUser.setUserName(registerDto.getRealName());
        sysUser.setNickName(registerDto.getRealName());
        sysUser.setPassword(SecurityUtils.encryptPassword(registerDto.getPwd()));
        R<?> registerResult = remoteUserService.registerUserInfo(sysUser, SecurityConstants.INNER);

        if (R.FAIL == registerResult.getCode())
        {
            throw new ServiceException(registerResult.getMsg());
        }


        VeoInfo veoInfo = new VeoInfo();
        veoInfo.setCarId(car.getId());
        veoInfo.setUserId(sysUser.getUserId());
        veoInfo.setIdentityNum(registerDto.getIdentityNum());

        return veoInfoMapper.insertVeoInfo(veoInfo);
    }

    /**
     * 查询车主个人信息
     *
     * @param id 车主个人信息主键
     * @return 车主个人信息
     */
    @Override
    public VeoInfo selectVeoInfoById(Long id)
    {
        return veoInfoMapper.selectVeoInfoById(id);
    }

    /**
     * 查询车主个人信息列表
     *
     * @param veoInfo 车主个人信息
     * @return 车主个人信息
     */
    @Override
    public List<VeoInfo> selectVeoInfoList(VeoInfo veoInfo)
    {
        return veoInfoMapper.selectVeoInfoList(veoInfo);
    }

    /**
     * 新增车主个人信息
     *
     * @param veoInfo 车主个人信息
     * @return 结果
     */
    @Override
    public int insertVeoInfo(VeoInfo veoInfo)
    {
        return veoInfoMapper.insertVeoInfo(veoInfo);
    }

    /**
     * 修改车主个人信息
     *
     * @param veoInfo 车主个人信息
     * @return 结果
     */
    @Override
    public int updateVeoInfo(VeoInfo veoInfo)
    {
        return veoInfoMapper.updateVeoInfo(veoInfo);
    }

    /**
     * 批量删除车主个人信息
     *
     * @param ids 需要删除的车主个人信息主键
     * @return 结果
     */
    @Override
    public int deleteVeoInfoByIds(Long[] ids)
    {
        return veoInfoMapper.deleteVeoInfoByIds(ids);
    }

    /**
     * 删除车主个人信息信息
     *
     * @param id 车主个人信息主键
     * @return 结果
     */
    @Override
    public int deleteVeoInfoById(Long id)
    {
        return veoInfoMapper.deleteVeoInfoById(id);
    }


}
