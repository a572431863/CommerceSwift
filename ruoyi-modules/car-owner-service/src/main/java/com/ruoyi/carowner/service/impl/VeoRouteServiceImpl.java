package com.ruoyi.carowner.service.impl;

import java.util.Date;
import java.util.List;

import com.ruoyi.carowner.domain.VeoInfo;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteDetailVo;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteQueryDto;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteBasicVo;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteOwnVo;
import com.ruoyi.carowner.mapper.VeoInfoMapper;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.carowner.mapper.VeoRouteMapper;
import com.ruoyi.carowner.domain.VeoRoute;
import com.ruoyi.carowner.service.IVeoRouteService;

/**
 * 车主路线Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-07
 */
@Service
public class VeoRouteServiceImpl implements IVeoRouteService {
    @Autowired
    private VeoRouteMapper veoRouteMapper;

    @Autowired
    private VeoInfoMapper veoInfoMapper;

    /**
     * 查询车主路线
     *
     * @param id 车主路线主键
     * @return 车主路线
     */
    @Override
    public VeoRoute selectVeoRouteById(Long id)
    {
        return veoRouteMapper.selectVeoRouteById(id);
    }

    /**
     * 查询车主路线列表
     *
     * @param veoRoute 车主路线
     * @return 车主路线
     */
    @Override
    public List<VeoRoute> selectVeoRouteList(VeoRoute veoRoute)
    {
        return veoRouteMapper.selectVeoRouteList(veoRoute);
    }

    /**
     * 查询车主及车辆类型
     * @param
     * @return
     */
    @Override
    public List<RouteOwnVo> selectRouteOwnVo() {
        return veoRouteMapper.selectRouteOwnVo(SecurityUtils.getUserId());
    }

    /**
     * 新增车主路线
     *
     * @param veoRoute 车主路线
     * @return 结果
     */
    @Override
    public int insertVeoRoute(VeoRoute veoRoute)
    {
        Long veoId = veoInfoMapper.getVeoId(SecurityUtils.getUserId());
        veoRoute.setVeoId(veoId);
        veoRoute.setGmtCreate(new Date());
        veoRoute.setGmtModified(new Date());
        return veoRouteMapper.insertVeoRoute(veoRoute);
    }

    /**
     * 修改车主路线
     *
     * @param veoRoute 车主路线
     * @return 结果
     */
    @Override
    public int updateVeoRoute(VeoRoute veoRoute)
    {
        veoRoute.setGmtModified(new Date());
        return veoRouteMapper.updateVeoRoute(veoRoute);
    }

    /**
     * 批量删除车主路线
     *
     * @param ids 需要删除的车主路线主键
     * @return 结果
     */
    @Override
    public int deleteVeoRouteByIds(Long[] ids)
    {
        return veoRouteMapper.deleteVeoRouteByIds(ids);
    }

    /**
     * 删除车主路线信息
     *
     * @param id 车主路线主键
     * @return 结果
     */
    @Override
    public int deleteVeoRouteById(Long id)
    {
        return veoRouteMapper.deleteVeoRouteById(id);
    }

    /**
     * 查询所有路线信息
     *
     */
    @Override
    public List<RouteBasicVo> showAll(RouteQueryDto routeQueryDto) {
        return veoRouteMapper.showAll(routeQueryDto);
    }

    /**
     * 查询单条路线详情
     *
     * @param routeId
     */
    @Override
    public RouteDetailVo findById(Long routeId) {
        return veoRouteMapper.findById(routeId);
    }
}
