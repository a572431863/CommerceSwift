package com.ruoyi.carowner.service.impl;

import java.util.List;

import com.ruoyi.carowner.domain.dto.vehicleManagement.CarVo;
import com.ruoyi.common.core.utils.IdWorker;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.carowner.mapper.CarMapper;
import com.ruoyi.carowner.domain.Car;
import com.ruoyi.carowner.service.ICarService;

/**
 * 车辆信息Service业务层处理
 *
 * @author wen
 * @date 2024-03-07
 */
@Service
public class CarServiceImpl implements ICarService {
    @Autowired
    private CarMapper carMapper;

    /**
     * 查询车辆信息
     *
     * @param id 车辆信息主键
     * @return 车辆信息
     */
    @Override
    public Car selectCarById(Long id)
    {
        return carMapper.selectCarById(id);
    }

    /**
     * 查询车辆信息列表
     *
     * @param car 车辆信息
     * @return 车辆信息
     */
    @Override
    public List<Car> selectCarList(Car car)
    {
        return carMapper.selectCarList(car);
    }

    /**
     * 多表查询车辆管理信息
     * @return
     */
    @Override
    public CarVo getCarInfo() {
        return carMapper.getCarInfo(SecurityUtils.getUserId());
    }

    /**
     * 新增车辆信息
     *
     * @param car 车辆信息
     * @return 结果
     */
    @Override
    public int insertCar(Car car)
    {
         car.setId(IdWorker.nextId());
        return carMapper.insertCar(car);
    }

    /**
     * 修改车辆信息
     *
     * @param car 车辆信息
     * @return 结果
     */
    @Override
    public int updateCar(Car car)
    {
        return carMapper.updateCar(car);
    }

    /**
     * 批量删除车辆信息
     *
     * @param ids 需要删除的车辆信息主键
     * @return 结果
     */
    @Override
    public int deleteCarByIds(Long[] ids)
    {
        int byIds = carMapper.deleteCarByIds(ids);
        return byIds;
    }

    /**
     * 删除车辆信息信息
     *
     * @param id 车辆信息主键
     * @return 结果
     */
    @Override
    public int deleteCarById(Long id)
    {
        return carMapper.deleteCarById(id);
    }
}
