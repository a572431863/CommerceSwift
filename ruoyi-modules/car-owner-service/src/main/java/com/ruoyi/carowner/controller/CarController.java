package com.ruoyi.carowner.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.carowner.domain.Car;
import com.ruoyi.carowner.service.ICarService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车辆信息Controller
 *
 * @author wen
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/car")
public class CarController extends BaseController {
    @Autowired
    private ICarService carService;

    /**
     * 查询车辆信息列表
     */
    @RequiresPermissions("carowner:car:list")
    @GetMapping("/list")
    public TableDataInfo list(Car car) {
        startPage();
        List<Car> list = carService.selectCarList(car);
        return getDataTable(list);
    }

    /**
     * 导出车辆信息列表
     */
    @RequiresPermissions("carowner:car:export")
    @Log(title = "车辆信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Car car) {
        List<Car> list = carService.selectCarList(car);
        ExcelUtil<Car> util = new ExcelUtil<Car>(Car.class);
        util.exportExcel(response, list, "车辆信息数据");
    }

    /**
     * 获取车辆信息详细信息
     */
    @RequiresPermissions("carowner:car:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(carService.selectCarById(id));
    }

    /**
     * 通过登录id获取车主个人及车辆具体信息
     */
    @RequiresPermissions("carowner:car:querys")
    @GetMapping("/info")
    public  AjaxResult getCarInfo(){
        return success(carService.getCarInfo());
    }

    /**
     * 新增车辆信息
     */
    @RequiresPermissions("carowner:car:add")
    @Log(title = "车辆信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Car car) {
        // 生成雪花ID并设置给car对象
//        car.setId(snowflake.nextId());
        return toAjax(carService.insertCar(car));
    }

    /**
     * 修改车辆信息
     */
    @RequiresPermissions("carowner:car:edit")
    @Log(title = "车辆信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Car car) {
        return toAjax(carService.updateCar(car));
    }

    /**
     * 删除车辆信息
     */
    @RequiresPermissions("carowner:car:remove")
    @Log(title = "车辆信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(carService.deleteCarByIds(ids));
    }
}
