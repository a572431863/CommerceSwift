package com.ruoyi.carowner.mapper;

import java.util.List;
import com.ruoyi.carowner.domain.VeoInfo;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 车主个人信息Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-07
 */
public interface VeoInfoMapper
{
    /**
     * 查询车主个人信息
     *
     * @param id 车主个人信息主键
     * @return 车主个人信息
     */
    public VeoInfo selectVeoInfoById(Long id);

    /**
     * 查询车主个人信息列表
     *
     * @param veoInfo 车主个人信息
     * @return 车主个人信息集合
     */
    public List<VeoInfo> selectVeoInfoList(VeoInfo veoInfo);

    /**
     * 新增车主个人信息
     *
     * @param veoInfo 车主个人信息
     * @return 结果
     */
    public int insertVeoInfo(VeoInfo veoInfo);



    /**
     * 修改车主个人信息
     *
     * @param veoInfo 车主个人信息
     * @return 结果
     */
    public int updateVeoInfo(VeoInfo veoInfo);

    /**
     * 删除车主个人信息
     *
     * @param id 车主个人信息主键
     * @return 结果
     */
    public int deleteVeoInfoById(Long id);

    /**
     * 批量删除车主个人信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVeoInfoByIds(Long[] ids);

    /**
     * 根据当前登录用户id获取对应车主信息
     */

    @Select("select id from veo_info where user_id = #{userId}")
    public Long getVeoId(@PathVariable("userId") Long userId);
}
