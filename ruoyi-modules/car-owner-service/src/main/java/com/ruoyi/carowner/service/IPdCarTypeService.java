package com.ruoyi.carowner.service;

import com.ruoyi.carowner.domain.PdCarType;

import java.util.List;

/**
 * 车辆类型Service接口
 *
 * @author ruoyi
 * @date 2024-03-08
 */
public interface IPdCarTypeService
{
    /**
     * 查询车辆类型
     *
     * @param id 车辆类型主键
     * @return 车辆类型
     */
    public PdCarType selectPdCarTypeById(Long id);

    /**
     * 查询车辆类型列表
     *
     * @param pdCarType 车辆类型
     * @return 车辆类型集合
     */
    public List<PdCarType> selectPdCarTypeList(PdCarType pdCarType);

    /**
     * 新增车辆类型
     *
     * @param pdCarType 车辆类型
     * @return 结果
     */
    public int insertPdCarType(PdCarType pdCarType);

    /**
     * 修改车辆类型
     *
     * @param pdCarType 车辆类型
     * @return 结果
     */
    public int updatePdCarType(PdCarType pdCarType);

    /**
     * 批量删除车辆类型
     *
     * @param ids 需要删除的车辆类型主键集合
     * @return 结果
     */
    public int deletePdCarTypeByIds(Long[] ids);

    /**
     * 删除车辆类型信息
     *
     * @param id 车辆类型主键
     * @return 结果
     */
    public int deletePdCarTypeById(Long id);
}
