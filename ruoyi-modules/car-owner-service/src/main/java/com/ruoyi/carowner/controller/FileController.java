package com.ruoyi.carowner.controller;

import com.ruoyi.carowner.config.Result;
import com.ruoyi.carowner.utils.AliOssUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/common")
@Slf4j
public class FileController {
    // 自动装配阿里云oss工具类
    @Autowired
    private AliOssUtil aliOssUtil;
    @PostMapping("/upload")
    @ApiOperation("文件上传")
    @CrossOrigin
    public Result<String> uploadImage(@RequestParam("picture") MultipartFile file){
        log.info("文件上传：{}",file);

        try {
            //原始文件名
            String originalFilename = file.getOriginalFilename();
            //截取文件名后缀  xxx.png
            String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
            //构造新文件名称1
            String url =  UUID.randomUUID().toString() + extension;
            //返回文件请求路径
            String filePath = aliOssUtil.upload(file.getBytes(), url);
            System.out.println(filePath);
            /*Map<String,Object> map = new HashMap<>();
            map.put("message", "上传图片成功!" );
            map.put("url", filePath);*/
            return Result.fail(filePath);
        } catch (IOException e) {
            log.error("文件上传失败");
            e.printStackTrace();
        }
        return Result.fail("文件上传失败");
    }
}
