package com.ruoyi.carowner.service.impl;

import com.ruoyi.carowner.domain.VeoShip;
import com.ruoyi.carowner.mapper.VeoShipMapper;
import com.ruoyi.carowner.service.IVeoShipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 运输类型Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-08
 */
@Service
public class VeoShipServiceImpl implements IVeoShipService
{
    @Autowired
    private VeoShipMapper veoShipMapper;

    /**
     * 查询运输类型
     *
     * @param id 运输类型主键
     * @return 运输类型
     */
    @Override
    public VeoShip selectVeoShipById(Long id)
    {
        return veoShipMapper.selectVeoShipById(id);
    }

    /**
     * 查询运输类型列表
     *
     * @param veoShip 运输类型
     * @return 运输类型
     */
    @Override
    public List<VeoShip> selectVeoShipList(VeoShip veoShip)
    {
        return veoShipMapper.selectVeoShipList(veoShip);
    }

    /**
     * 新增运输类型
     *
     * @param veoShip 运输类型
     * @return 结果
     */
    @Override
    public int insertVeoShip(VeoShip veoShip)
    {
        return veoShipMapper.insertVeoShip(veoShip);
    }

    /**
     * 修改运输类型
     *
     * @param veoShip 运输类型
     * @return 结果
     */
    @Override
    public int updateVeoShip(VeoShip veoShip)
    {
        return veoShipMapper.updateVeoShip(veoShip);
    }

    /**
     * 批量删除运输类型
     *
     * @param ids 需要删除的运输类型主键
     * @return 结果
     */
    @Override
    public int deleteVeoShipByIds(Long[] ids)
    {
        return veoShipMapper.deleteVeoShipByIds(ids);
    }

    /**
     * 删除运输类型信息
     *
     * @param id 运输类型主键
     * @return 结果
     */
    @Override
    public int deleteVeoShipById(Long id)
    {
        return veoShipMapper.deleteVeoShipById(id);
    }
}
