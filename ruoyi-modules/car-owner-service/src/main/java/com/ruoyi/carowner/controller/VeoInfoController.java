package com.ruoyi.carowner.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.carowner.domain.dto.vehicleManagement.VeoRegisterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.carowner.domain.VeoInfo;
import com.ruoyi.carowner.service.IVeoInfoService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 车主个人信息Controller
 *
 * @author ruoyi
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/info")
public class VeoInfoController extends BaseController
{
    @Autowired
    private IVeoInfoService veoInfoService;

    /**
     * 查询车主个人信息列表
     */
    @RequiresPermissions("carowner:info:list")
    @GetMapping("/list")
    public TableDataInfo list(VeoInfo veoInfo)
    {
        startPage();
        List<VeoInfo> list = veoInfoService.selectVeoInfoList(veoInfo);
        return getDataTable(list);
    }

    /**
     * 导出车主个人信息列表
     */
    @RequiresPermissions("carowner:info:export")
    @Log(title = "车主个人信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, VeoInfo veoInfo)
    {
        List<VeoInfo> list = veoInfoService.selectVeoInfoList(veoInfo);
        ExcelUtil<VeoInfo> util = new ExcelUtil<VeoInfo>(VeoInfo.class);
        util.exportExcel(response, list, "车主个人信息数据");
    }

    /**
     * 获取车主个人信息详细信息
     */
    @RequiresPermissions("carowner:info:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(veoInfoService.selectVeoInfoById(id));
    }

    /**
     * 新增车主个人信息
     */
    @RequiresPermissions("carowner:info:add")
    @Log(title = "车主个人信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VeoInfo veoInfo)
    {
        return toAjax(veoInfoService.insertVeoInfo(veoInfo));
    }

    /**
     * 修改车主个人信息
     */
    @RequiresPermissions("carowner:info:edit")
    @Log(title = "车主个人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody VeoInfo veoInfo)
    {
        return toAjax(veoInfoService.updateVeoInfo(veoInfo));
    }

    /**
     * 删除车主个人信息
     */
    @RequiresPermissions("carowner:info:remove")
    @Log(title = "车主个人信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(veoInfoService.deleteVeoInfoByIds(ids));
    }


    /**
     * 注册车主
     */
    @Log(title = "车主注册信息", businessType = BusinessType.REGISTRATION)
    @PostMapping("/registration")
    public AjaxResult registration(@RequestBody VeoRegisterDto registerDto)
    {
        return toAjax(veoInfoService.insertVeoInfo(registerDto));
    }
}
