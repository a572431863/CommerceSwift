package com.ruoyi.carowner.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.carowner.domain.Car;
import com.ruoyi.carowner.domain.dto.vehicleManagement.CarVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 车辆信息Mapper接口
 *
 * @author wen
 * @date 2024-03-07
 */
@Mapper
public interface CarMapper {
    /**
     * 查询车辆信息
     *
     * @param id 车辆信息主键
     * @return 车辆信息
     */

    public Car selectCarById(Long id);

    /**
     * 查询车辆信息列表
     *
     * @param car 车辆信息
     * @return 车辆信息集合
     */
    public List<Car> selectCarList(Car car);

    /**
     * 新增车辆信息
     *
     * @param car 车辆信息
     * @return 结果
     */
    public int insertCar(Car car);

    /**
     * 修改车辆信息
     *
     * @param car 车辆信息
     * @return 结果
     */
    public int updateCar(Car car);
    /**
     * 车辆管理查询
     */
    @Select("SELECT c.plate_num, c.img, s.nick_name, s.phonenumber, p.size, t.type FROM veo_info d\n" +
            "JOIN veo_car c ON d.car_id = c.id JOIN sys_user s ON d.user_id = s.user_id JOIN pd_car_size p\n" +
            "ON c.size_id = p.id JOIN pd_car_type t ON c.type_id = t.id where s.user_id = #{userId}")
     public CarVo getCarInfo(@PathVariable("userId") Long userId);
    /**
     * 删除车辆信息
     *
     * @param id 车辆信息主键
     * @return 结果
     */
    public int deleteCarById(Long id);

    /**
     * 批量删除车辆信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarByIds(Long[] ids);

    /**
     * 通过车牌号获取车辆表中的id
     */
    @Select("SELECT id FROM veo_car where plate_num = #{plateNum}")
    Long getCarId(@PathVariable("plateNum") String plateNum);
}
