package com.ruoyi.carowner.domain.dto.vehicleRouting;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
/**
 * 车辆路线查询条件
 */
public class RouteQueryDto {

    /**
     * 到达地
     */
    private String arrival;

    /**
     * 目的地
     */
    private String departure;

    /**
     * 车辆类型外键
     */
    private Long typeId;

    /**
     * 车辆长度外键
     */
    private Long sizeId;

    /**
     * 运输类型
     */
    private Long shipId;
}
