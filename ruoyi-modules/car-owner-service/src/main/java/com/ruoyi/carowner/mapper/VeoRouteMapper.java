package com.ruoyi.carowner.mapper;

import java.util.List;
import com.ruoyi.carowner.domain.VeoRoute;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteDetailVo;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteQueryDto;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteBasicVo;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteOwnVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 车主路线Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-07
 */
public interface VeoRouteMapper
{
    /**
     * 查询车主路线
     *
     * @param id 车主路线主键
     * @return 车主路线
     */
    public VeoRoute selectVeoRouteById(Long id);

    /**
     * 通过登录id查询车主发布的路线及类型
     */
    @Select("SELECT r.id, arrival, departure,v.brief FROM veo_ship v JOIN veo_route r ON r.ship_type=v.id \n" +
            "join veo_info i on i.id = r.veo_id join sys_user u on u.user_id = i.user_id where u.user_id = #{userId} \n" +
            "ORDER BY r.gmt_modified DESC")
    public List<RouteOwnVo> selectRouteOwnVo(@PathVariable("userId") Long userId);
    /**
     * 查询车主路线列表
     *
     * @param veoRoute 车主路线
     * @return 车主路线集合
     */
    public List<VeoRoute> selectVeoRouteList(VeoRoute veoRoute);

    /**
     * 新增车主路线
     *
     * @param veoRoute 车主路线
     * @return 结果
     */
    public int insertVeoRoute(VeoRoute veoRoute);

    /**
     * 修改车主路线
     *
     * @param veoRoute 车主路线
     * @return 结果
     */
    public int updateVeoRoute(VeoRoute veoRoute);

    /**
     * 删除车主路线
     *
     * @param id 车主路线主键
     * @return 结果
     */
    public int deleteVeoRouteById(Long id);

    /**
     * 批量删除车主路线
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVeoRouteByIds(Long[] ids);

    /**
     * 查询所有车主路线，包含车辆及出发地、目的地等信息
     */
     List<RouteBasicVo> showAll(@Param("q") RouteQueryDto routeQueryDto);

    /**
     * 查询单条路线详情
     */
    @Select("SELECT r.id AS routeId, arrival, departure, c.plate_num, img, t.type, su.nick_name, phonenumber, sz.size \n" +
            "FROM veo_route r JOIN veo_ship s ON r.ship_type = s.id JOIN veo_car c ON r.veo_id = c.id JOIN pd_car_type t \n" +
            "ON c.type_id = t.id JOIN pd_car_size sz ON c.size_id = sz.id JOIN veo_info d ON c.id = d.car_id \n" +
            "JOIN sys_user su ON d.user_id = su.user_id where r.id = #{routeId}")
    RouteDetailVo findById(@PathVariable("routeId") Long routeId);
}
