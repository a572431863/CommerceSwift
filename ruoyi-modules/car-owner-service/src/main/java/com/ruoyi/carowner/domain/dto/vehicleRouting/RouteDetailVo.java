package com.ruoyi.carowner.domain.dto.vehicleRouting;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
/**
 * 路线详情信息
 */
public class RouteDetailVo extends BaseEntity {

    /**
     * 路线id
     */
    private Long routeId;

    /**
     * 到达地
     */
    private String arrival;

    /**
     * 出发地
     */
    private String departure;

    /**
     * 车辆类型
     */
    private String type;

    @Excel(name = "车辆长度")
    private Double size;

    @Excel(name = "车牌号")
    private String plateNum;

    /**
     * 车主真实姓名
     */
    private String nickName;

    /**
     * 车主联系电话
     */
    private String phoneNumber;

    /**
     * 车辆图片
     */
    private String img;
}
