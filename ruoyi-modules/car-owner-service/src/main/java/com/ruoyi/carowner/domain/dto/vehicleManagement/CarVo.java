package com.ruoyi.carowner.domain.dto.vehicleManagement;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 车辆管理列表
 */
@Data
@Getter
@Setter
public class CarVo extends BaseEntity {
    private static final long serialVersionUID = 1L;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long carId;
    private String plateNum;
    private String img;

    private Long userId;
    private String nickName;
    private String phoneNumber;

    private Long sizeId;
    private String size;

    private Long typeId;
    private String type;

}
