package com.ruoyi.carowner.service.impl;

import com.ruoyi.carowner.domain.PdCarType;
import com.ruoyi.carowner.mapper.PdCarTypeMapper;
import com.ruoyi.carowner.service.IPdCarTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 车辆类型Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-08
 */
@Service
public class PdCarTypeServiceImpl implements IPdCarTypeService
{
    @Autowired
    private PdCarTypeMapper pdCarTypeMapper;

    /**
     * 查询车辆类型
     *
     * @param id 车辆类型主键
     * @return 车辆类型
     */
    @Override
    public PdCarType selectPdCarTypeById(Long id)
    {
        return pdCarTypeMapper.selectPdCarTypeById(id);
    }

    /**
     * 查询车辆类型列表
     *
     * @param pdCarType 车辆类型
     * @return 车辆类型
     */
    @Override
    public List<PdCarType> selectPdCarTypeList(PdCarType pdCarType)
    {
        return pdCarTypeMapper.selectPdCarTypeList(pdCarType);
    }

    /**
     * 新增车辆类型
     *
     * @param pdCarType 车辆类型
     * @return 结果
     */
    @Override
    public int insertPdCarType(PdCarType pdCarType)
    {
        return pdCarTypeMapper.insertPdCarType(pdCarType);
    }

    /**
     * 修改车辆类型
     *
     * @param pdCarType 车辆类型
     * @return 结果
     */
    @Override
    public int updatePdCarType(PdCarType pdCarType)
    {
        return pdCarTypeMapper.updatePdCarType(pdCarType);
    }

    /**
     * 批量删除车辆类型
     *
     * @param ids 需要删除的车辆类型主键
     * @return 结果
     */
    @Override
    public int deletePdCarTypeByIds(Long[] ids)
    {
        return pdCarTypeMapper.deletePdCarTypeByIds(ids);
    }

    /**
     * 删除车辆类型信息
     *
     * @param id 车辆类型主键
     * @return 结果
     */
    @Override
    public int deletePdCarTypeById(Long id)
    {
        return pdCarTypeMapper.deletePdCarTypeById(id);
    }
}
