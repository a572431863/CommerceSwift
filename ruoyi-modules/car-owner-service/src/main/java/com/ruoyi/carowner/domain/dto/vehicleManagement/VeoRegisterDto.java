package com.ruoyi.carowner.domain.dto.vehicleManagement;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;



/**
 * 车主注册信息
 */
@Data
@Getter
@Setter
public class VeoRegisterDto {

    /** 身份证号 */
    private String identityNum;

    /** 姓名 */
    private String realName;

    /** 手机号码 */
    private String phoneNumber;

    /** 车牌号 */
    private String plateNum;

    /** 车辆图片 */
    private String img;

    /** 车辆类型 */
    private Long typeId;

    /** 车辆长度 */
    private Long sizeId;

    /** 密码 */
    private String pwd;

}
