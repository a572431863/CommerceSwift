package com.ruoyi.carowner.config;


import com.ruoyi.carowner.utils.AliOssProperties;
import com.ruoyi.carowner.utils.AliOssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//配置类，创建AliOssUtils对象
@Configuration
@Slf4j
public class OSSConfiguration {

    @Value("{aliyun.oss.endpoint}")
    private String endpoint;
    @Value("{aliyun.oss.keyId}")
    private String accessKeyId;
    @Value("{aliyun.oss.keySecret}")
    private String accessKeySecret;
    @Value("{aliyun.oss.bucketName}")
    private String bucketName;

    //注入bean AliOssUtil
    //AliOssProperties是已经注入的bean，作为aliOssUtil的参数
    @Bean
    @ConditionalOnMissingBean
    public AliOssUtil aliOssUtil(AliOssProperties aliOssProperties){
        log.info("开始注入阿里云oss文件上传工具类对象：{}",aliOssProperties);
        return new AliOssUtil(endpoint,accessKeyId,
                accessKeySecret,bucketName );
    }
}
