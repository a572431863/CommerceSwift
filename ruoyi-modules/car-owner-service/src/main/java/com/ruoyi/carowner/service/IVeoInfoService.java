package com.ruoyi.carowner.service;

import java.util.List;
import com.ruoyi.carowner.domain.VeoInfo;
import com.ruoyi.carowner.domain.dto.vehicleManagement.VeoRegisterDto;

/**
 * 车主个人信息Service接口
 *
 * @author ruoyi
 * @date 2024-03-07
 */
public interface IVeoInfoService
{
    /**
     * 查询车主个人信息
     *
     * @param id 车主个人信息主键
     * @return 车主个人信息
     */
    public VeoInfo selectVeoInfoById(Long id);

    /**
     * 查询车主个人信息列表
     *
     * @param veoInfo 车主个人信息
     * @return 车主个人信息集合
     */
    public List<VeoInfo> selectVeoInfoList(VeoInfo veoInfo);

    /**
     * 新增车主个人信息
     *
     * @param veoInfo 车主个人信息
     * @return 结果
     */
    public int insertVeoInfo(VeoInfo veoInfo);

    /**
     * 修改车主个人信息
     *
     * @param veoInfo 车主个人信息
     * @return 结果
     */
    public int updateVeoInfo(VeoInfo veoInfo);

    /**
     * 批量删除车主个人信息
     *
     * @param ids 需要删除的车主个人信息主键集合
     * @return 结果
     */
    public int deleteVeoInfoByIds(Long[] ids);

    /**
     * 删除车主个人信息信息
     *
     * @param id 车主个人信息主键
     * @return 结果
     */
    public int deleteVeoInfoById(Long id);

    /**
     * 新增车主个人信息
     *
     * @param registerDto 车主注册信息
     * @return 结果
     */
    public int insertVeoInfo(VeoRegisterDto registerDto);
}
