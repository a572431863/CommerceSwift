package com.ruoyi.carowner.service.impl;

import com.ruoyi.carowner.domain.CarSize;
import com.ruoyi.carowner.mapper.CarSizeMapper;
import com.ruoyi.carowner.service.ICarSizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 车辆长度Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-08
 */
@Service
public class CarSizeServiceImpl implements ICarSizeService
{
    @Autowired
    private CarSizeMapper carSizeMapper;

    /**
     * 查询车辆长度
     *
     * @param id 车辆长度主键
     * @return 车辆长度
     */
    @Override
    public CarSize selectCarSizeById(Long id)
    {
        return carSizeMapper.selectCarSizeById(id);
    }

    /**
     * 查询车辆长度列表
     *
     * @param carSize 车辆长度
     * @return 车辆长度
     */
    @Override
    public List<CarSize> selectCarSizeList(CarSize carSize)
    {
        return carSizeMapper.selectCarSizeList(carSize);
    }

    /**
     * 新增车辆长度
     *
     * @param carSize 车辆长度
     * @return 结果
     */
    @Override
    public int insertCarSize(CarSize carSize)
    {
        return carSizeMapper.insertCarSize(carSize);
    }

    /**
     * 修改车辆长度
     *
     * @param carSize 车辆长度
     * @return 结果
     */
    @Override
    public int updateCarSize(CarSize carSize)
    {
        return carSizeMapper.updateCarSize(carSize);
    }

    /**
     * 批量删除车辆长度
     *
     * @param ids 需要删除的车辆长度主键
     * @return 结果
     */
    @Override
    public int deleteCarSizeByIds(Long[] ids)
    {
        return carSizeMapper.deleteCarSizeByIds(ids);
    }

    /**
     * 删除车辆长度信息
     *
     * @param id 车辆长度主键
     * @return 结果
     */
    @Override
    public int deleteCarSizeById(Long id)
    {
        return carSizeMapper.deleteCarSizeById(id);
    }
}
