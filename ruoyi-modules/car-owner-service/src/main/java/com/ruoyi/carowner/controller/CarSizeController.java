package com.ruoyi.carowner.controller;

import com.ruoyi.carowner.domain.CarSize;
import com.ruoyi.carowner.service.ICarSizeService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 车辆长度Controller
 *
 * @author ruoyi
 * @date 2024-03-08
 */
@RestController
@RequestMapping("/size")
public class CarSizeController extends BaseController
{
    @Autowired
    private ICarSizeService carSizeService;

    /**
     * 查询车辆长度列表
     */
    @RequiresPermissions("veo:size:list")
    @GetMapping("/list")
    public TableDataInfo list(CarSize carSize)
    {
        startPage();
        List<CarSize> list = carSizeService.selectCarSizeList(carSize);
        return getDataTable(list);
    }

    /**
     * 导出车辆长度列表
     */
    @RequiresPermissions("veo:size:export")
    @Log(title = "车辆长度", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CarSize carSize)
    {
        List<CarSize> list = carSizeService.selectCarSizeList(carSize);
        ExcelUtil<CarSize> util = new ExcelUtil<CarSize>(CarSize.class);
        util.exportExcel(response, list, "车辆长度数据");
    }

    /**
     * 获取车辆长度详细信息
     */
    @RequiresPermissions("veo:size:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(carSizeService.selectCarSizeById(id));
    }

    /**
     * 新增车辆长度
     */
    @RequiresPermissions("veo:size:add")
    @Log(title = "车辆长度", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CarSize carSize)
    {
        return toAjax(carSizeService.insertCarSize(carSize));
    }

    /**
     * 修改车辆长度
     */
    @RequiresPermissions("veo:size:edit")
    @Log(title = "车辆长度", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CarSize carSize)
    {
        return toAjax(carSizeService.updateCarSize(carSize));
    }

    /**
     * 删除车辆长度
     */
    @RequiresPermissions("veo:size:remove")
    @Log(title = "车辆长度", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(carSizeService.deleteCarSizeByIds(ids));
    }
}
