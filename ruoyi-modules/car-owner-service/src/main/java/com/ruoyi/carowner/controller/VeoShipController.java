package com.ruoyi.carowner.controller;

import com.ruoyi.carowner.domain.VeoShip;
import com.ruoyi.carowner.service.IVeoShipService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 运输类型Controller
 *
 * @author ruoyi
 * @date 2024-03-08
 */
@RestController
@RequestMapping("/ship")
public class VeoShipController extends BaseController
{
    @Autowired
    private IVeoShipService veoShipService;

    /**
     * 查询运输类型列表
     */
    @RequiresPermissions("platform:ship:list")
    @GetMapping("/list")
    public TableDataInfo list(VeoShip veoShip)
    {
        startPage();
        List<VeoShip> list = veoShipService.selectVeoShipList(veoShip);
        return getDataTable(list);
    }

    /**
     * 导出运输类型列表
     */
    @RequiresPermissions("platform:ship:export")
    @Log(title = "运输类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, VeoShip veoShip)
    {
        List<VeoShip> list = veoShipService.selectVeoShipList(veoShip);
        ExcelUtil<VeoShip> util = new ExcelUtil<VeoShip>(VeoShip.class);
        util.exportExcel(response, list, "运输类型数据");
    }

    /**
     * 获取运输类型详细信息
     */
    @RequiresPermissions("platform:ship:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(veoShipService.selectVeoShipById(id));
    }

    /**
     * 新增运输类型
     */
    @RequiresPermissions("platform:ship:add")
    @Log(title = "运输类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody VeoShip veoShip)
    {
        return toAjax(veoShipService.insertVeoShip(veoShip));
    }

    /**
     * 修改运输类型
     */
    @RequiresPermissions("platform:ship:edit")
    @Log(title = "运输类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody VeoShip veoShip)
    {
        return toAjax(veoShipService.updateVeoShip(veoShip));
    }

    /**
     * 删除运输类型
     */
    @RequiresPermissions("platform:ship:remove")
    @Log(title = "运输类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(veoShipService.deleteVeoShipByIds(ids));
    }
}
