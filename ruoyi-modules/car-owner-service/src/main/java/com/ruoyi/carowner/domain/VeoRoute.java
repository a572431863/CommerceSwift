package com.ruoyi.carowner.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车主路线对象 veo_route
 *
 * @author ruoyi
 * @date 2024-03-07
 */
public class VeoRoute extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 到达地
     */
    @Excel(name = "到达地")
    private String arrival;

    /**
     * 出发地
     */
    @Excel(name = "出发地")
    private String departure;

    /**
     * 关联车主表veo_detail主键
     */
    @Excel(name = "关联车主表veo_detail主键")
    private Long veoId;

    /**
     * 关联运输类型表veo_ship主键
     */
    @Excel(name = "关联运输类型表veo_ship主键")
    private Long shipType;

    /**
     * 是否被公开显示，默认值为1显示；2不显示（如该路线已匹配到货主需求并确认，则只有对应的货主才能看到这条路线）
     */
    @Excel(name = "是否被公开显示，默认值为1显示；2不显示", readConverterExp = "如=该路线已匹配到货主需求并确认，则只有对应的货主才能看到这条路线")
    private String isShowed;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    /**
     * 1被删除，0未被删除
     */
    @Excel(name = "1被删除，0未被删除")
    private Long isDeleted;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getArrival() {
        return arrival;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDeparture() {
        return departure;
    }

    public void setVeoId(Long veoId) {
        this.veoId = veoId;
    }

    public Long getVeoId() {
        return veoId;
    }

    public void setShipType(Long shipType) {
        this.shipType = shipType;
    }

    public Long getShipType() {
        return shipType;
    }

    public void setIsShowed(String isShowd) {
        this.isShowed = isShowd;
    }

    public String getIsShowed() {
        return isShowed;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setIsDeleted(Long isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getIsDeleted() {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("arrival", getArrival())
                .append("departure", getDeparture())
                .append("veoId", getVeoId())
                .append("shipType", getShipType())
                .append("isShowed", getIsShowed())
                .append("gmtCreate", getGmtCreate())
                .append("gmtModified", getGmtModified())
                .append("isDeleted", getIsDeleted())
                .toString();
    }
}
