package com.ruoyi.carowner.service;

import java.util.List;
import com.ruoyi.carowner.domain.Car;
import com.ruoyi.carowner.domain.dto.vehicleManagement.CarVo;

/**
 * 车辆信息Service接口
 *
 * @author wen
 * @date 2024-03-07
 */
public interface ICarService {
    /**
     * 查询车辆信息
     *
     * @param id 车辆信息主键
     * @return 车辆信息
     */
    public Car selectCarById(Long id);

    /**
     * 查询车辆信息列表
     *
     * @param car 车辆信息
     * @return 车辆信息集合
     */
    public List<Car> selectCarList(Car car);
    /**
     * 多表联查车辆管理信息
     */
    public CarVo getCarInfo();

    /**
     * 新增车辆信息
     *
     * @param car 车辆信息
     * @return 结果
     */
    public int insertCar(Car car);

    /**
     * 修改车辆信息
     *
     * @param car 车辆信息
     * @return 结果
     */
    public int updateCar(Car car);

    /**
     * 批量删除车辆信息
     *
     * @param ids 需要删除的车辆信息主键集合
     * @return 结果
     */
    public int deleteCarByIds(Long[] ids);

    /**
     * 删除车辆信息信息
     *
     * @param id 车辆信息主键
     * @return 结果
     */
    public int deleteCarById(Long id);
}
