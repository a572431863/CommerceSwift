package com.ruoyi.carowner.mapper;

import com.ruoyi.carowner.domain.VeoShip;

import java.util.List;

/**
 * 运输类型Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-08
 */
public interface VeoShipMapper
{
    /**
     * 查询运输类型
     *
     * @param id 运输类型主键
     * @return 运输类型
     */
    public VeoShip selectVeoShipById(Long id);

    /**
     * 查询运输类型列表
     *
     * @param veoShip 运输类型
     * @return 运输类型集合
     */
    public List<VeoShip> selectVeoShipList(VeoShip veoShip);

    /**
     * 新增运输类型
     *
     * @param veoShip 运输类型
     * @return 结果
     */
    public int insertVeoShip(VeoShip veoShip);

    /**
     * 修改运输类型
     *
     * @param veoShip 运输类型
     * @return 结果
     */
    public int updateVeoShip(VeoShip veoShip);

    /**
     * 删除运输类型
     *
     * @param id 运输类型主键
     * @return 结果
     */
    public int deleteVeoShipById(Long id);

    /**
     * 批量删除运输类型
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteVeoShipByIds(Long[] ids);
}
