package com.ruoyi.carowner.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车辆信息对象 veo_car
 *
 * @author wen
 * @date 2024-03-07
 */
@Data
@Getter
@Setter
public class Car extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;


    /** 关联车辆类型表veo_car_type主键 */
    @Excel(name = "关联车辆类型表veo_car_type主键")
    private Long typeId;

    /** 关联车辆高度表veo_car_size主键 */
    @Excel(name = "关联车辆高度表veo_car_size主键")
    private Long sizeId;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String plateNum;

    /** 车辆照片 */
    @Excel(name = "车辆照片")
    @TableField("img")
    private String img;
    /** 逻辑删除  */
    @Excel(name = "逻辑删除")
    private Integer isDeleted;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTypeId(Long typeId)
    {
        this.typeId = typeId;
    }

    public Long getTypeId()
    {
        return typeId;
    }
    public void setSizeId(Long sizeId)
    {
        this.sizeId = sizeId;
    }

    public Long getSizeId()
    {
        return sizeId;
    }
    public void setPlateNum(String plateNum)
    {
        this.plateNum = plateNum;
    }

    public String getPlateNum()
    {
        return plateNum;
    }
    public void setImg(String img)
    {
        this.img = img;
    }

    public String getImg()
    {
        return img;
    }
    public Integer getIsDeleted()
    {
        return isDeleted;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("typeId", getTypeId())
            .append("sizeId", getSizeId())
            .append("plateNum", getPlateNum())
            .append("img", getImg())
            .append("isDeleted",getIsDeleted())
            .toString();
    }
}
