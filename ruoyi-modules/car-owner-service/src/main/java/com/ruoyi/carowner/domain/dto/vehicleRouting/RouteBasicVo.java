package com.ruoyi.carowner.domain.dto.vehicleRouting;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Data
@Getter
@Setter
/**
 * 车主路线基本信息
 */
public class RouteBasicVo extends BaseEntity {
    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @Excel(name = "到达地")
    private String arrival;

    @Excel(name = "出发地")
    private String departure;

    @Excel(name = "车辆类型")
    private String type;

    @Excel(name = "车辆长度")
    private Double size;

    @Excel(name = "车牌号")
    private String plateNum;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    @Excel(name = "运输类型")
    private String brief;
}
