package com.ruoyi.carowner.controller;

import com.ruoyi.carowner.domain.PdCarType;
import com.ruoyi.carowner.service.IPdCarTypeService;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 车辆类型Controller
 *
 * @author ruoyi
 * @date 2024-03-08
 */
@RestController
@RequestMapping("/type")
public class PdCarTypeController extends BaseController
{
    @Autowired
    private IPdCarTypeService pdCarTypeService;

    /**
     * 查询车辆类型列表
     */
    @RequiresPermissions("platform:type:list")
    @GetMapping("/list")
    public TableDataInfo list(PdCarType pdCarType)
    {
        startPage();
        List<PdCarType> list = pdCarTypeService.selectPdCarTypeList(pdCarType);
        return getDataTable(list);
    }

    /**
     * 导出车辆类型列表
     */
    @RequiresPermissions("platform:type:export")
    @Log(title = "车辆类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PdCarType pdCarType)
    {
        List<PdCarType> list = pdCarTypeService.selectPdCarTypeList(pdCarType);
        ExcelUtil<PdCarType> util = new ExcelUtil<PdCarType>(PdCarType.class);
        util.exportExcel(response, list, "车辆类型数据");
    }

    /**
     * 获取车辆类型详细信息
     */
    @RequiresPermissions("platform:type:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pdCarTypeService.selectPdCarTypeById(id));
    }

    /**
     * 新增车辆类型
     */
    @RequiresPermissions("platform:type:add")
    @Log(title = "车辆类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PdCarType pdCarType)
    {
        return toAjax(pdCarTypeService.insertPdCarType(pdCarType));
    }

    /**
     * 修改车辆类型
     */
    @RequiresPermissions("platform:type:edit")
    @Log(title = "车辆类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PdCarType pdCarType)
    {
        return toAjax(pdCarTypeService.updatePdCarType(pdCarType));
    }

    /**
     * 删除车辆类型
     */
    @RequiresPermissions("platform:type:remove")
    @Log(title = "车辆类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(pdCarTypeService.deletePdCarTypeByIds(ids));
    }
}
