package com.ruoyi.carowner.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 车辆长度对象 pd_car_size
 *
 * @author ruoyi
 * @date 2024-03-08
 */
public class CarSize extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 车辆长度（米），如5.5, 4.2等 */
    @Excel(name = "车辆长度", readConverterExp = "米=")
    private BigDecimal size;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSize(BigDecimal size)
    {
        this.size = size;
    }

    public BigDecimal getSize()
    {
        return size;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("size", getSize())
            .toString();
    }
}
