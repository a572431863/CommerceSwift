package com.ruoyi.carowner.mapper;

import com.ruoyi.carowner.domain.CarSize;

import java.util.List;

/**
 * 车辆长度Mapper接口
 *
 * @author ruoyi
 * @date 2024-03-08
 */
public interface CarSizeMapper
{
    /**
     * 查询车辆长度
     *
     * @param id 车辆长度主键
     * @return 车辆长度
     */
    public CarSize selectCarSizeById(Long id);

    /**
     * 查询车辆长度列表
     *
     * @param carSize 车辆长度
     * @return 车辆长度集合
     */
    public List<CarSize> selectCarSizeList(CarSize carSize);

    /**
     * 新增车辆长度
     *
     * @param carSize 车辆长度
     * @return 结果
     */
    public int insertCarSize(CarSize carSize);

    /**
     * 修改车辆长度
     *
     * @param carSize 车辆长度
     * @return 结果
     */
    public int updateCarSize(CarSize carSize);

    /**
     * 删除车辆长度
     *
     * @param id 车辆长度主键
     * @return 结果
     */
    public int deleteCarSizeById(Long id);

    /**
     * 批量删除车辆长度
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarSizeByIds(Long[] ids);
}
