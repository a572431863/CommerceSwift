package com.ruoyi.carowner.service;

import java.util.List;
import com.ruoyi.carowner.domain.VeoRoute;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteDetailVo;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteQueryDto;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteBasicVo;
import com.ruoyi.carowner.domain.dto.vehicleRouting.RouteOwnVo;

/**
 * 车主路线Service接口
 *
 * @author ruoyi
 * @date 2024-03-07
 */
public interface IVeoRouteService
{
    /**
     * 查询车主路线
     *
     * @param id 车主路线主键
     * @return 车主路线
     */
    public VeoRoute selectVeoRouteById(Long id);

    /**
     * 查询车主路线列表
     *
     * @param veoRoute 车主路线
     * @return 车主路线集合
     */
    public List<VeoRoute> selectVeoRouteList(VeoRoute veoRoute);

    /**
     * 查询车主类型及类型
     * @param
     * @return
     */
    public List<RouteOwnVo> selectRouteOwnVo();
    /**
     * 新增车主路线
     *
     * @param veoRoute 车主路线
     * @return 结果
     */
    public int insertVeoRoute(VeoRoute veoRoute);

    /**
     * 修改车主路线
     *
     * @param veoRoute 车主路线
     * @return 结果
     */
    public int updateVeoRoute(VeoRoute veoRoute);

    /**
     * 批量删除车主路线
     *
     * @param ids 需要删除的车主路线主键集合
     * @return 结果
     */
    public int deleteVeoRouteByIds(Long[] ids);

    /**
     * 删除车主路线信息
     *
     * @param id 车主路线主键
     * @return 结果
     */
    public int deleteVeoRouteById(Long id);

    /**
     * 查询所有路线信息
     */
    public List<RouteBasicVo> showAll(RouteQueryDto routeQueryDto);

    /**
     * 查询单条路线详情
     */
    RouteDetailVo findById(Long routeId);
}
