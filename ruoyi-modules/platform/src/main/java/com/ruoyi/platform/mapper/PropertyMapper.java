package com.ruoyi.platform.mapper;

import java.util.List;
import com.ruoyi.platform.domain.Property;

/**
 * 属性Mapper接口
 * 
 * @author ruoyi
 * @date 2024-03-11
 */
public interface PropertyMapper 
{
    /**
     * 查询属性
     * 
     * @param id 属性主键
     * @return 属性
     */
    public Property selectPropertyById(Long id);

    /**
     * 查询属性列表
     * 
     * @param property 属性
     * @return 属性集合
     */
    public List<Property> selectPropertyList(Property property);

    /**
     * 新增属性
     * 
     * @param property 属性
     * @return 结果
     */
    public int insertProperty(Property property);

    /**
     * 修改属性
     * 
     * @param property 属性
     * @return 结果
     */
    public int updateProperty(Property property);

    /**
     * 删除属性
     * 
     * @param id 属性主键
     * @return 结果
     */
    public int deletePropertyById(Long id);

    /**
     * 批量删除属性
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePropertyByIds(Long[] ids);
}
