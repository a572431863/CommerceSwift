package com.ruoyi.platform.mapper;

import com.ruoyi.common.model.platform.dto.OrderDto;
import com.ruoyi.platform.domain.Order;
import com.ruoyi.platform.domain.OrderDetails;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单Mapper接口
 * 
 * @author 梁栩
 * @date 2024-03-06
 */
public interface OrderMapper 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public Order selectOrderById(Long id);

    /**
     * 查询订单列表
     * 
     * @param order 订单
     * @return 订单集合
     */
    public List<Order> selectOrderList(Order order);

    /**
     * 新增订单
     * 
     * @param order 订单
     * @return 结果
     */
    public int insertOrder(Order order);

    /**
     * 修改订单
     * 
     * @param order 订单
     * @return 结果
     */
    public int updateOrder(Order order);

    /**
     * 删除订单
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteOrderById(Long id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderByIds(Long[] ids);

    /**
     * 批量删除订单详细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderDetailsByOrderIds(Long[] ids);
    
    /**
     * 批量新增订单详细
     * 
     * @param orderDetailsList 订单详细列表
     * @return 结果
     */
    public int batchOrderDetails(List<OrderDetails> orderDetailsList);
    

    /**
     * 通过订单主键删除订单详细信息
     * 
     * @param id 订单ID
     * @return 结果
     */
    public int deleteOrderDetailsByOrderId(Long id);




    List<OrderDto> find(@Param("Or")OrderDto orderDto);
}
