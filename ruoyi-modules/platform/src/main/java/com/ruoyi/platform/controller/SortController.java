package com.ruoyi.platform.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.platform.domain.Sort;
import com.ruoyi.platform.service.ISortService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;

/**
 * 类目管理Controller
 * 
 * @author 梁栩
 * @date 2024-03-14
 */
@RestController
@RequestMapping("/sort")
public class SortController extends BaseController
{
    @Autowired
    private ISortService sortService;

    /**
     * 查询类目管理列表
     */
    @RequiresPermissions("platform:sort:list")
    @GetMapping("/list")
    public AjaxResult list(Sort sort)
    {
        List<Sort> list = sortService.selectSortList(sort);
        return success(list);
    }

    /**
     * 导出类目管理列表
     */
    @RequiresPermissions("platform:sort:export")
    @Log(title = "类目管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Sort sort)
    {
        List<Sort> list = sortService.selectSortList(sort);
        ExcelUtil<Sort> util = new ExcelUtil<Sort>(Sort.class);
        util.exportExcel(response, list, "类目管理数据");
    }

    /**
     * 获取类目管理详细信息
     */
    @RequiresPermissions("platform:sort:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(sortService.selectSortById(id));
    }

    /**
     * 新增类目管理
     */
    @RequiresPermissions("platform:sort:add")
    @Log(title = "类目管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Sort sort)
    {
        return toAjax(sortService.insertSort(sort));
    }

    /**
     * 修改类目管理
     */
    @RequiresPermissions("platform:sort:edit")
    @Log(title = "类目管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Sort sort)
    {
        return toAjax(sortService.updateSort(sort));
    }

    /**
     * 删除类目管理
     */
    @RequiresPermissions("platform:sort:remove")
    @Log(title = "类目管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sortService.deleteSortByIds(ids));
    }
}
