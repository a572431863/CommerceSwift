package com.ruoyi.platform.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 商品管理对象 plf_product
 *
 * @author ruoyi
 * @date 2024-03-07
 */
public class Product extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 商品编号
     */
    @Excel(name = "商品编号")
    private String productCode;

    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    private String productName;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sortId;

    /**
     * 图片
     */
    @Excel(name = "图片")
    private String image;

    /**
     * 商品服务
     */
    @Excel(name = "商品服务")
    private Long productService;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long deleteFlag;

    /**
     * 商品状态
     */
    @Excel(name = "商品状态")
    private Long state;

    /**
     * 销量
     */
    @Excel(name = "销量")
    private Long buyCount;

    /**
     * 浏览量
     */
    @Excel(name = "浏览量")
    private Long view;

    //库存
    private Long stock;

    public void setStock(Long state){
        this.stock = state;
    }
    public Long getStock(){
        return stock;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setSortId(Long sortId) {
        this.sortId = sortId;
    }

    public Long getSortId() {
        return sortId;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setProductService(Long productService) {
        this.productService = productService;
    }

    public Long getProductService() {
        return productService;
    }

    public void setDeleteFlag(Long deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() {
        return deleteFlag;
    }

    public void setState(Long state) {
        this.state = state;
    }

    public Long getState() {
        return state;
    }

    public void setBuyCount(Long buyCount) {
        this.buyCount = buyCount;
    }

    public Long getBuyCount() {
        return buyCount;
    }

    public void setView(Long view) {
        this.view = view;
    }

    public Long getView() {
        return view;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("productCode", getProductCode())
                .append("productName", getProductName())
                .append("sortId", getSortId())
                .append("image", getImage())
                .append("productService", getProductService())
                .append("updateTime", getUpdateTime())
                .append("createTime", getCreateTime())
                .append("deleteFlag", getDeleteFlag())
                .append("state", getState())
                .append("buyCount", getBuyCount())
                .append("view", getView())
                .toString();
    }
}
