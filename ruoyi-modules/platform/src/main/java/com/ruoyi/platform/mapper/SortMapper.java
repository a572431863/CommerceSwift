package com.ruoyi.platform.mapper;

import java.util.List;
import com.ruoyi.platform.domain.Sort;

/**
 * 类目管理Mapper接口
 * 
 * @author 梁栩
 * @date 2024-03-14
 */
public interface SortMapper 
{
    /**
     * 查询类目管理
     * 
     * @param id 类目管理主键
     * @return 类目管理
     */
    public Sort selectSortById(Long id);

    /**
     * 查询类目管理列表
     * 
     * @param sort 类目管理
     * @return 类目管理集合
     */
    public List<Sort> selectSortList(Sort sort);

    /**
     * 新增类目管理
     * 
     * @param sort 类目管理
     * @return 结果
     */
    public int insertSort(Sort sort);

    /**
     * 修改类目管理
     * 
     * @param sort 类目管理
     * @return 结果
     */
    public int updateSort(Sort sort);

    /**
     * 删除类目管理
     * 
     * @param id 类目管理主键
     * @return 结果
     */
    public int deleteSortById(Long id);

    /**
     * 批量删除类目管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSortByIds(Long[] ids);
}
