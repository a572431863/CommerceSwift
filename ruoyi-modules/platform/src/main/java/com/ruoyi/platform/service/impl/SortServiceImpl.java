package com.ruoyi.platform.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.platform.mapper.SortMapper;
import com.ruoyi.platform.domain.Sort;
import com.ruoyi.platform.service.ISortService;

/**
 * 类目管理Service业务层处理
 * 
 * @author 梁栩
 * @date 2024-03-14
 */
@Service
public class SortServiceImpl implements ISortService 
{
    @Autowired
    private SortMapper sortMapper;

    /**
     * 查询类目管理
     * 
     * @param id 类目管理主键
     * @return 类目管理
     */
    @Override
    public Sort selectSortById(Long id)
    {
        return sortMapper.selectSortById(id);
    }

    /**
     * 查询类目管理列表
     * 
     * @param sort 类目管理
     * @return 类目管理
     */
    @Override
    public List<Sort> selectSortList(Sort sort)
    {
        return sortMapper.selectSortList(sort);
    }

    /**
     * 新增类目管理
     * 
     * @param sort 类目管理
     * @return 结果
     */
    @Override
    public int insertSort(Sort sort)
    {
        sort.setCreateTime(DateUtils.getNowDate());
        sort.setId(IdWorker.nextId());
        return sortMapper.insertSort(sort);
    }

    /**
     * 修改类目管理
     * 
     * @param sort 类目管理
     * @return 结果
     */
    @Override
    public int updateSort(Sort sort)
    {
        sort.setUpdateTime(DateUtils.getNowDate());
        return sortMapper.updateSort(sort);
    }

    /**
     * 批量删除类目管理
     * 
     * @param ids 需要删除的类目管理主键
     * @return 结果
     */
    @Override
    public int deleteSortByIds(Long[] ids)
    {
        return sortMapper.deleteSortByIds(ids);
    }

    /**
     * 删除类目管理信息
     * 
     * @param id 类目管理主键
     * @return 结果
     */
    @Override
    public int deleteSortById(Long id)
    {
        return sortMapper.deleteSortById(id);
    }
}
