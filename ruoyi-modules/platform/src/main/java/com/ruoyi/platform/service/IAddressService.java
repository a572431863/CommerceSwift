package com.ruoyi.platform.service;

import java.util.List;
import com.ruoyi.platform.domain.Address;

/**
 * addressService接口
 * 
 * @author wyh
 * @date 2024-03-08
 */
public interface IAddressService 
{
    /**
     * 查询address
     * 
     * @param id address主键
     * @return address
     */
    public Address selectAddressById(Long id);

    /**
     * 查询address列表
     * 
     * @param address address
     * @return address集合
     */
    public List<Address> selectAddressList(Address address);

    /**
     * 新增address
     * 
     * @param address address
     * @return 结果
     */
    public int insertAddress(Address address);

    /**
     * 修改address
     * 
     * @param address address
     * @return 结果
     */
    public int updateAddress(Address address);

    /**
     * 批量删除address
     * 
     * @param ids 需要删除的address主键集合
     * @return 结果
     */
    public int deleteAddressByIds(Long[] ids);

    /**
     * 删除address信息
     * 
     * @param id address主键
     * @return 结果
     */
    public int deleteAddressById(Long id);
}
