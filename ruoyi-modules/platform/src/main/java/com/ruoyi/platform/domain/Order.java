package com.ruoyi.platform.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单对象 plf_order
 *
 * @author 梁栩
 * @date 2024-03-06
 */

public class Order extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 订单编号
     */
    @Excel(name = "订单编号")
    private String orderNum;

    /**
     * 总价格
     */
    @Excel(name = "总价格")
    private BigDecimal totalAmount;

    /**
     * 订单状态（0 待付款，1 待收货，2 已完成，3 已取消）
     */
    @Excel(name = "订单状态", readConverterExp = "0=,待=付款，1,待=收货，2,已=完成，3,已=取消")
    private Long state;

    /**
     * 商品数量
     */
    @Excel(name = "商品数量")
    private Long productConut;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long deleteFlag;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long userId;

    /**
     * 订单详细信息
     */
    private List<OrderDetails> orderDetailsList;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setState(Long state) {
        this.state = state;
    }

    public Long getState() {
        return state;
    }

    public void setProductConut(Long productConut) {
        this.productConut = productConut;
    }

    public Long getProductConut() {
        return productConut;
    }

    public void setDeleteFlag(Long deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() {
        return deleteFlag;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public List<OrderDetails> getOrderDetailsList() {
        return orderDetailsList;
    }

    public void setOrderDetailsList(List<OrderDetails> orderDetailsList) {
        this.orderDetailsList = orderDetailsList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("orderNum", getOrderNum())
                .append("totalAmount", getTotalAmount())
                .append("state", getState())
                .append("productConut", getProductConut())
                .append("updateTime", getUpdateTime())
                .append("createTime", getCreateTime())
                .append("deleteFlag", getDeleteFlag())
                .append("userId", getUserId())
                .append("orderDetailsList", getOrderDetailsList())
                .toString();
    }
}
