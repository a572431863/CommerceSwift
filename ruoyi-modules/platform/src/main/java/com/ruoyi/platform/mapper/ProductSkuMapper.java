package com.ruoyi.platform.mapper;

import java.util.List;
import com.ruoyi.platform.domain.ProductSku;

/**
 * 商品sukMapper接口
 * 
 * @author 梁栩
 * @date 2024-03-08
 */
public interface ProductSkuMapper 
{
    /**
     * 查询商品suk
     * 
     * @param id 商品suk主键
     * @return 商品suk
     */
    public ProductSku selectProductSkuById(Long id);

    /**
     * 查询商品suk列表
     * 
     * @param productSku 商品suk
     * @return 商品suk集合
     */
    public List<ProductSku> selectProductSkuList(ProductSku productSku);

    /**
     * 新增商品suk
     * 
     * @param productSku 商品suk
     * @return 结果
     */
    public int insertProductSku(ProductSku productSku);

    /**
     * 修改商品suk
     * 
     * @param productSku 商品suk
     * @return 结果
     */
    public int updateProductSku(ProductSku productSku);

    /**
     * 删除商品suk
     * 
     * @param id 商品suk主键
     * @return 结果
     */
    public int deleteProductSkuById(Long id);

    /**
     * 批量删除商品suk
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductSkuByIds(Long[] ids);
}
