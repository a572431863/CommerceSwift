package com.ruoyi.platform.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.platform.mapper.ProductSkuMapper;
import com.ruoyi.platform.domain.ProductSku;
import com.ruoyi.platform.service.IProductSkuService;

/**
 * 商品sukService业务层处理
 * 
 * @author 梁栩
 * @date 2024-03-08
 */
@Service
public class ProductSkuServiceImpl implements IProductSkuService 
{
    @Autowired
    private ProductSkuMapper productSkuMapper;

    /**
     * 查询商品suk
     * 
     * @param id 商品suk主键
     * @return 商品suk
     */
    @Override
    public ProductSku selectProductSkuById(Long id)
    {
        return productSkuMapper.selectProductSkuById(id);
    }

    /**
     * 查询商品suk列表
     * 
     * @param productSku 商品suk
     * @return 商品suk
     */
    @Override
    public List<ProductSku> selectProductSkuList(ProductSku productSku)
    {
        return productSkuMapper.selectProductSkuList(productSku);
    }

    /**
     * 新增商品suk
     * 
     * @param productSku 商品suk
     * @return 结果
     */
    @Override
    public int insertProductSku(ProductSku productSku)
    {
        productSku.setCreateTime(DateUtils.getNowDate());
        return productSkuMapper.insertProductSku(productSku);
    }

    /**
     * 修改商品suk
     * 
     * @param productSku 商品suk
     * @return 结果
     */
    @Override
    public int updateProductSku(ProductSku productSku)
    {
        productSku.setUpdateTime(DateUtils.getNowDate());
        return productSkuMapper.updateProductSku(productSku);
    }

    /**
     * 批量删除商品suk
     * 
     * @param ids 需要删除的商品suk主键
     * @return 结果
     */
    @Override
    public int deleteProductSkuByIds(Long[] ids)
    {
        return productSkuMapper.deleteProductSkuByIds(ids);
    }

    /**
     * 删除商品suk信息
     * 
     * @param id 商品suk主键
     * @return 结果
     */
    @Override
    public int deleteProductSkuById(Long id)
    {
        return productSkuMapper.deleteProductSkuById(id);
    }
}
