package com.ruoyi.platform.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@EnableScheduling
public class DelayedMessageHandler {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Scheduled(fixedDelay = 1000) // 定时任务，每秒执行一次
    public void processDelayedMessages() {
        long currentTime = System.currentTimeMillis();
        Set<String> messages = redisTemplate.opsForZSet().rangeByScore("delayed_queue", 0, currentTime);
        if (messages != null) {
            for (String message : messages) {
                // 处理消息
                handleMessage(message);
                // 从 Sorted Set 中移除已处理的消息
                redisTemplate.opsForZSet().remove("delayed_queue", message);
            }
        }
    }

    public void handleMessage(String message) {
        System.out.println("Received delayed message: " + message);
    }
}
