package com.ruoyi.platform.service.impl;

import java.util.Date;

import cn.hutool.core.util.RandomUtil;
import com.ruoyi.common.core.enums.impl.BusinessCode;
import com.ruoyi.common.core.exception.Assert;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.IdWorker;
import com.ruoyi.common.core.utils.JwtUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.utils.bean.BeanUtils;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.model.platform.dto.OrderAddDto;
import com.ruoyi.common.model.platform.dto.OrderDto;
import com.ruoyi.common.model.platform.dto.PaymentDto;
import com.ruoyi.platform.domain.Address;
import com.ruoyi.platform.domain.Order;
import com.ruoyi.platform.domain.OrderDetails;
import com.ruoyi.platform.domain.Product;
import com.ruoyi.platform.mapper.AddressMapper;
import com.ruoyi.platform.mapper.OrderDetailsMapper;
import com.ruoyi.platform.mapper.OrderMapper;
import com.ruoyi.platform.mapper.ProductMapper;
import com.ruoyi.platform.model.dto.ProductDto;
import com.ruoyi.platform.service.IOrderService;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.domain.SysUser;
import org.apache.catalina.User;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单Service业务层处理
 *
 * @author 梁栩
 * @date 2024-03-06
 */
@Service
public class OrderServiceImpl implements IOrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderDetailsMapper orderDetailsMapper;
    @Autowired
    private AddressMapper addressMapper;
    @Autowired
    private RemoteUserService remoteUserService;

    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private RedissonClient redissonClient;


    @Transactional
    @Override
    public void payment(PaymentDto paymentDto) {
        //根据orderId查询订单详情
        OrderDetails orderDetails = orderDetailsMapper.selectOrderDetailsByOrderId(paymentDto.getOrderId());
        //设置下单时间为现在
        orderDetails.setPaymentTime(new Date());


        //根据orderId 查询订单
        Order order = orderMapper.selectOrderById(paymentDto.getOrderId());
        //修改订单状态为待收货
        order.setState(1L);

        ProductDto productDto = productMapper.selectProductById(orderDetails.getProductId());
        Product product = new Product();
        BeanUtils.copyBeanProp(product, productDto);
        product.setBuyCount(product.getBuyCount() + 1);
        product.setView(product.getView() + 1);


        product.setStock(product.getStock() - order.getProductConut());
        productMapper.updateProduct(product);

        //修改订单详情表
        orderDetailsMapper.updateOrderDetails(orderDetails);
        //修改订单
        orderMapper.updateOrder(order);


    }

    @Transactional
    @Override
    public void addOrder(OrderAddDto orderAddDto, String userId) {

        ProductDto productDto = productMapper.selectProductById(orderAddDto.getProductId());
        //获取锁
        RLock lock = redissonClient.getLock("Lock_" + userId);
        try {
            //加锁
            lock.lock();
            Assert.error(orderAddDto.getProductConut() > productDto.getStock(),
                    BusinessCode.SECKILL_PRODUCT_SOLD_OUT);
        } finally {
            //释放锁
            lock.unlock();
        }
        //给order赋值
        Order order = new Order();
        order.setId(IdWorker.nextId());
        order.setOrderNum(RandomUtil.randomNumbers(8));
        order.setTotalAmount(orderAddDto.getTotalAmount());
        order.setState(0L);
        order.setProductConut(orderAddDto.getProductConut());
        order.setUserId(Long.parseLong(userId));
        //添加
        orderMapper.insertOrder(order);

        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setId(IdWorker.nextId());
        orderDetails.setAddressId(0L);
        orderDetails.setPaymentTime(new Date());
        orderDetails.setOrderId(order.getId());
        orderDetails.setProductId(orderAddDto.getProductId());
        orderDetailsMapper.insertOrderDetails(orderDetails);

        SysUser sysUser = remoteUserService.findById(Long.parseLong(userId));

        Address address = new Address();
        address.setId(IdWorker.nextId());
        address.setProvince(orderAddDto.getProvince());
        address.setCity(orderAddDto.getCity());
        address.setArea(orderAddDto.getArea());
        address.setDetailAddress(orderAddDto.getDetailAddress());
        address.setTel(sysUser.getPhonenumber());
        address.setReciver("0");
        address.setEmailCode(sysUser.getEmail());
        address.setStatus("1");
        address.setIsDefault("1");
        address.setUserId(sysUser.getUserId());

        addressMapper.insertAddress(address);

    }


    /**
     * 多表查询
     */
    @Override
    public List<OrderDto> find(OrderDto orderDto) {
        return orderMapper.find(orderDto);
    }


    /**
     * 查询订单
     *
     * @param id 订单主键
     * @return 订单
     */
    @Override
    public Order selectOrderById(Long id) {
        return orderMapper.selectOrderById(id);
    }

    /**
     * 查询订单列表
     *
     * @param order 订单
     * @return 订单
     */
    @Override
    public List<Order> selectOrderList(Order order) {
        return orderMapper.selectOrderList(order);
    }

    /**
     * 新增订单
     *
     * @param order 订单
     * @return 结果
     */
    @Transactional
    @Override
    public int insertOrder(Order order) {
        order.setCreateTime(DateUtils.getNowDate());
        int rows = orderMapper.insertOrder(order);
        insertOrderDetails(order);
        return rows;
    }

    /**
     * 修改订单
     *
     * @param order 订单
     * @return 结果
     */
    @Transactional
    @Override
    public int updateOrder(Order order) {
        order.setUpdateTime(DateUtils.getNowDate());
        orderMapper.deleteOrderDetailsByOrderId(order.getId());
        insertOrderDetails(order);
        return orderMapper.updateOrder(order);
    }

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的订单主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteOrderByIds(Long[] ids) {
        orderMapper.deleteOrderDetailsByOrderIds(ids);
        return orderMapper.deleteOrderByIds(ids);
    }

    /**
     * 删除订单信息
     *
     * @param id 订单主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteOrderById(Long id) {
        orderMapper.deleteOrderDetailsByOrderId(id);
        return orderMapper.deleteOrderById(id);
    }


    /**
     * 新增订单详细信息
     *
     * @param order 订单对象
     */
    public void insertOrderDetails(Order order) {
        List<OrderDetails> orderDetailsList = order.getOrderDetailsList();
        Long id = order.getId();
        if (StringUtils.isNotNull(orderDetailsList)) {
            List<OrderDetails> list = new ArrayList<OrderDetails>();
            for (OrderDetails orderDetails : orderDetailsList) {
                orderDetails.setOrderId(id);
                list.add(orderDetails);
            }
            if (list.size() > 0) {
                orderMapper.batchOrderDetails(list);
            }
        }
    }
}
