package com.ruoyi.platform.service;

import java.util.List;

import com.ruoyi.platform.domain.OrderDetails;

/**
 * 订单详细Service接口
 *
 * @author 梁栩
 * @date 2024-03-08
 */
public interface IOrderDetailsService {


    /**
     * 根据orderId 来查询详情
     */
    OrderDetails selectOrderDetailsByOrderId(Long orderId);

    /**
     * 查询订单详细
     *
     * @param id 订单详细主键
     * @return 订单详细
     */
    public OrderDetails selectOrderDetailsById(Long id);

    /**
     * 查询订单详细列表
     *
     * @param orderDetails 订单详细
     * @return 订单详细集合
     */
    public List<OrderDetails> selectOrderDetailsList(OrderDetails orderDetails);

    /**
     * 新增订单详细
     *
     * @param orderDetails 订单详细
     * @return 结果
     */
    public int insertOrderDetails(OrderDetails orderDetails);

    /**
     * 修改订单详细
     *
     * @param orderDetails 订单详细
     * @return 结果
     */
    public int updateOrderDetails(OrderDetails orderDetails);

    /**
     * 批量删除订单详细
     *
     * @param ids 需要删除的订单详细主键集合
     * @return 结果
     */
    public int deleteOrderDetailsByIds(Long[] ids);

    /**
     * 删除订单详细信息
     *
     * @param id 订单详细主键
     * @return 结果
     */
    public int deleteOrderDetailsById(Long id);
}
