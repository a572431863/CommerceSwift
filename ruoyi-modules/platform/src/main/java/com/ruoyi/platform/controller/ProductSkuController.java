package com.ruoyi.platform.controller;

import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.platform.domain.ProductSku;
import com.ruoyi.platform.service.IProductSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 商品sukController
 * 
 * @author 梁栩
 * @date 2024-03-08
 */
@RestController
@RequestMapping("/SKU")
public class ProductSkuController extends BaseController
{
    @Autowired
    private IProductSkuService productSkuService;

    /**
     * 查询商品suk列表
     */
    @RequiresPermissions("platform:SKU:list")
    @GetMapping("/list")
    public TableDataInfo list(ProductSku productSku)
    {
        startPage();
        List<ProductSku> list = productSkuService.selectProductSkuList(productSku);
        return getDataTable(list);
    }

    /**
     * 导出商品suk列表
     */
    @RequiresPermissions("platform:SKU:export")
    @Log(title = "商品suk", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductSku productSku)
    {
        List<ProductSku> list = productSkuService.selectProductSkuList(productSku);
        ExcelUtil<ProductSku> util = new ExcelUtil<ProductSku>(ProductSku.class);
        util.exportExcel(response, list, "商品suk数据");
    }

    /**
     * 获取商品suk详细信息
     */
    @RequiresPermissions("platform:SKU:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(productSkuService.selectProductSkuById(id));
    }

    /**
     * 新增商品suk
     */
    @RequiresPermissions("platform:SKU:add")
    @Log(title = "商品suk", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductSku productSku)
    {
        return toAjax(productSkuService.insertProductSku(productSku));
    }

    /**
     * 修改商品suk
     */
    @RequiresPermissions("platform:SKU:edit")
    @Log(title = "商品suk", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductSku productSku)
    {
        return toAjax(productSkuService.updateProductSku(productSku));
    }

    /**
     * 删除商品suk
     */
    @RequiresPermissions("platform:SKU:remove")
    @Log(title = "商品suk", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(productSkuService.deleteProductSkuByIds(ids));
    }
}
