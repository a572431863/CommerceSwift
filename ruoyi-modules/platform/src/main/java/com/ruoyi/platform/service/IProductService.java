package com.ruoyi.platform.service;

import java.util.List;

import com.ruoyi.common.model.platform.vo.ProductVo;
import com.ruoyi.platform.domain.Product;
import com.ruoyi.platform.model.dto.ProductDto;

/**
 * 商品管理Service接口
 * 
 * @author ruoyi
 * @date 2024-03-07
 */
public interface IProductService 
{
    /**
     * 查询商品管理
     *
     * @param id 商品管理主键
     * @return 商品管理
     */
    public ProductDto selectProductById(Long id);

    /**
     * 查询商品管理列表
     * 
     * @param product 商品管理
     * @return 商品管理集合
     */
    public List<Product> selectProductList(Product product);

    /**
     * 新增商品管理
     * 
     * @param product 商品管理
     * @return 结果
     */
    public int insertProduct(Product product);

    /**
     * 修改商品管理
     * 
     * @param product 商品管理
     * @return 结果
     */
    public int updateProduct(Product product);

    /**
     * 批量删除商品管理
     * 
     * @param ids 需要删除的商品管理主键集合
     * @return 结果
     */
    public int deleteProductByIds(Long[] ids);

    /**
     * 删除商品管理信息
     * 
     * @param id 商品管理主键
     * @return 结果
     */
    public int deleteProductById(Long id);

    List<ProductVo> top10();

}
