package com.ruoyi.platform.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 属性对象 plf_property
 * 
 * @author ruoyi
 * @date 2024-03-11
 */
public class Property extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /** 属性项名称 */
    @Excel(name = "属性项名称")
    private String propertyName;

    /** 是否多选 */
    @Excel(name = "是否多选")
    private Integer isMultiple;

    /** 属性值 */
    @Excel(name = "属性值")
    private String propertyValue;

    /** $column.columnComment */
    private Integer deleteFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPropertyName(String propertyName) 
    {
        this.propertyName = propertyName;
    }

    public String getPropertyName() 
    {
        return propertyName;
    }
    public void setIsMultiple(Integer isMultiple) 
    {
        this.isMultiple = isMultiple;
    }

    public Integer getIsMultiple() 
    {
        return isMultiple;
    }
    public void setPropertyValue(String propertyValue) 
    {
        this.propertyValue = propertyValue;
    }

    public String getPropertyValue() 
    {
        return propertyValue;
    }
    public void setDeleteFlag(Integer deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Integer getDeleteFlag() 
    {
        return deleteFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("propertyName", getPropertyName())
            .append("isMultiple", getIsMultiple())
            .append("propertyValue", getPropertyValue())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("deleteFlag", getDeleteFlag())
            .toString();
    }
}
