package com.ruoyi.platform.lineRunner;

import com.ruoyi.common.model.platform.vo.ProductVo;
import com.ruoyi.platform.domain.Product;
import com.ruoyi.platform.service.IProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ProductLineRunner implements CommandLineRunner {

    @Autowired
    private IProductService productService;
    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    public void run(String... args) throws Exception {

        List<ProductVo> products = productService.top10();
        products.stream().map(p -> {
            ProductVo productVo = new ProductVo();
            // 对象拷贝：参数1：数据源；参数2：目标
            BeanUtils.copyProperties(p,productVo);
            return productVo;
        }).forEach(product -> {
            redisTemplate.opsForZSet().add("TOP10_",product,product.getBuyCount());
        });
    }
}
