package com.ruoyi.platform.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.TreeEntity;

/**
 * 类目管理对象 plf_sort
 * 
 * @author 梁栩
 * @date 2024-03-14
 */
public class Sort extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 类型名称 */
    @Excel(name = "类型名称")
    private String typeName;

    /** 上级 */
    private String lelveId;

    /**  */
    private Long deleteFlag;

    /**  */
    @Excel(name = "图片")
    private String img;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTypeName(String typeName) 
    {
        this.typeName = typeName;
    }

    public String getTypeName() 
    {
        return typeName;
    }
    public void setLelveId(String lelveId) 
    {
        this.lelveId = lelveId;
    }

    public String getLelveId() 
    {
        return lelveId;
    }
    public void setDeleteFlag(Long deleteFlag) 
    {
        this.deleteFlag = deleteFlag;
    }

    public Long getDeleteFlag() 
    {
        return deleteFlag;
    }
    public void setImg(String img) 
    {
        this.img = img;
    }

    public String getImg() 
    {
        return img;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("typeName", getTypeName())
            .append("lelveId", getLelveId())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .append("deleteFlag", getDeleteFlag())
            .append("img", getImg())
            .toString();
    }
}
