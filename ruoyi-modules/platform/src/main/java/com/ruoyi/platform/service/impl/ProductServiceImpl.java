package com.ruoyi.platform.service.impl;

import java.util.List;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.IdWorker;
import com.ruoyi.common.model.platform.vo.ProductVo;
import com.ruoyi.platform.model.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.platform.mapper.ProductMapper;
import com.ruoyi.platform.domain.Product;
import com.ruoyi.platform.service.IProductService;

/**
 * 商品管理Service业务层处理
 *
 * @author ruoyi
 * @date 2024-03-07
 */
@Service
public class ProductServiceImpl implements IProductService {
    @Autowired
    private ProductMapper productMapper;

    /**
     * 查询商品管理
     *
     * @param id 商品管理主键
     * @return 商品管理
     */
    @Override
    public ProductDto selectProductById(Long id) {
        return productMapper.selectProductById(id);
    }

    /**
     * 查询商品管理列表
     *
     * @param product 商品管理
     * @return 商品管理
     */
    @Override
    public List<Product> selectProductList(Product product) {
        return productMapper.selectProductList(product);
    }

    /**
     * 新增商品管理
     *
     * @param product 商品管理
     * @return 结果
     */
    @Override
    public int insertProduct(Product product) {
        product.setCreateTime(DateUtils.getNowDate());
        product.setId(IdWorker.nextId());
        return productMapper.insertProduct(product);
    }

    /**
     * 修改商品管理
     *
     * @param product 商品管理
     * @return 结果
     */
    @Override
    public int updateProduct(Product product) {
        product.setUpdateTime(DateUtils.getNowDate());
        return productMapper.updateProduct(product);
    }

    /**
     * 批量删除商品管理
     *
     * @param ids 需要删除的商品管理主键
     * @return 结果
     */
    @Override
    public int deleteProductByIds(Long[] ids) {
        return productMapper.deleteProductByIds(ids);
    }

    /**
     * 删除商品管理信息
     *
     * @param id 商品管理主键
     * @return 结果
     */
    @Override
    public int deleteProductById(Long id) {

        return productMapper.deleteProductById(id);
    }

    @Override
    public List<ProductVo> top10() {
        List<ProductVo> productVos = productMapper.top10();
        return productVos;
    }
}
