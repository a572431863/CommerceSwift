package com.ruoyi.platform.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.utils.JwtUtils;
import com.ruoyi.common.model.platform.dto.OrderAddDto;
import com.ruoyi.common.model.platform.dto.OrderDto;
import com.ruoyi.common.model.platform.dto.PaymentDto;
import com.ruoyi.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.platform.domain.Order;
import com.ruoyi.platform.service.IOrderService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 订单Controller
 *
 * @author 梁栩
 * @date 2024-03-06
 */
@RestController
@RequestMapping("/order")
public class OrderController extends BaseController {
    @Autowired
    private IOrderService orderService;



    /**
     * 查询
     */
    @RequiresPermissions("platform:order:payment")
    @PostMapping("/payment")
    public AjaxResult payment(@RequestBody PaymentDto paymentDto) {
        orderService.payment(paymentDto);

        return null;
    }





    /**
     * 查询
     */
    @RequiresPermissions("platform:order:find")
    @PostMapping("/findPage")
    public AjaxResult findOrders(@RequestBody OrderDto orderDto) {
        startPage();
        List<OrderDto> orderDtos = orderService.find(orderDto);

        return success(orderDtos);
    }


    /**
     * 新增订单
     */
    @RequiresPermissions("platform:order:addOrder")
    @GetMapping("/addOrder")
    public AjaxResult addOrder(OrderAddDto orderAddDto) {
        String userId = String.valueOf(SecurityUtils.getUserId());
        orderService.addOrder(orderAddDto,userId);

        return AjaxResult.success();
    }




    /**
     * 查询订单列表
     */
    @RequiresPermissions("platform:order:list")
    @GetMapping("/list")
    public TableDataInfo list(Order order) {
        startPage();
        List<Order> list = orderService.selectOrderList(order);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @RequiresPermissions("platform:order:export")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Order order) {
        List<Order> list = orderService.selectOrderList(order);
        ExcelUtil<Order> util = new ExcelUtil<Order>(Order.class);
        util.exportExcel(response, list, "订单数据");
    }

    /**
     * 获取订单详细信息
     */
    @RequiresPermissions("platform:order:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(orderService.selectOrderById(id));
    }

    /**
     * 新增订单
     */
    @RequiresPermissions("platform:order:add")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Order order) {
        return toAjax(orderService.insertOrder(order));
    }



    /**
     * 修改订单
     */
    @RequiresPermissions("platform:order:edit")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Order order) {
        return toAjax(orderService.updateOrder(order));
    }

    /**
     * 删除订单
     */
    @RequiresPermissions("platform:order:remove")
    @Log(title = "订单", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(orderService.deleteOrderByIds(ids));
    }
}
