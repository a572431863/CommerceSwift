package com.ruoyi.platform.model.dto;

import com.ruoyi.common.core.annotation.Excel;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class ProductDto {
    private String propertyValue;
    private BigDecimal price;

    private BigDecimal weight;

    private Long id;

    /**
     * 商品编号
     */
    private String productCode;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * $column.columnComment
     */
    private Long sortId;

    /**
     * 图片
     */
    private String image;

    /**
     * 商品服务
     */
    private Long productService;

    /**
     * 商品状态
     */
    @Excel(name = "商品状态")
    private Long state;

    /**
     * 销量
     */
    @Excel(name = "销量")
    private Long buyCount;

    /**
     * 浏览量
     */
    @Excel(name = "浏览量")
    private Long view;

    /**
     * 库存
     */
    @Excel(name = "库存")
    private Long stock;
}
