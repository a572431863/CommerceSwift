package com.ruoyi.platform.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.platform.domain.Property;
import com.ruoyi.platform.service.IPropertyService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 属性Controller
 * 
 * @author ruoyi
 * @date 2024-03-11
 */
@RestController
@RequestMapping("/property")
public class PropertyController extends BaseController
{
    @Autowired
    private IPropertyService propertyService;

    /**
     * 查询属性列表
     */
    @RequiresPermissions("platform:property:list")
    @GetMapping("/list")
    public TableDataInfo list(Property property)
    {
        startPage();
        List<Property> list = propertyService.selectPropertyList(property);
        return getDataTable(list);
    }

    /**
     * 导出属性列表
     */
    @RequiresPermissions("platform:property:export")
    @Log(title = "属性", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Property property)
    {
        List<Property> list = propertyService.selectPropertyList(property);
        ExcelUtil<Property> util = new ExcelUtil<Property>(Property.class);
        util.exportExcel(response, list, "属性数据");
    }

    /**
     * 获取属性详细信息
     */
    @RequiresPermissions("platform:property:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(propertyService.selectPropertyById(id));
    }

    /**
     * 新增属性
     */
    @RequiresPermissions("platform:property:add")
    @Log(title = "属性", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Property property)
    {
        return toAjax(propertyService.insertProperty(property));
    }

    /**
     * 修改属性
     */
    @RequiresPermissions("platform:property:edit")
    @Log(title = "属性", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Property property)
    {
        return toAjax(propertyService.updateProperty(property));
    }

    /**
     * 删除属性
     */
    @RequiresPermissions("platform:property:remove")
    @Log(title = "属性", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(propertyService.deletePropertyByIds(ids));
    }
}
