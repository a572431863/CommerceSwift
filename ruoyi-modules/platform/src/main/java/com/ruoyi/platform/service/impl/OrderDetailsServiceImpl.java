package com.ruoyi.platform.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.platform.mapper.OrderDetailsMapper;
import com.ruoyi.platform.domain.OrderDetails;
import com.ruoyi.platform.service.IOrderDetailsService;

/**
 * 订单详细Service业务层处理
 * 
 * @author 梁栩
 * @date 2024-03-08
 */
@Service
public class OrderDetailsServiceImpl implements IOrderDetailsService 
{
    @Autowired
    private OrderDetailsMapper orderDetailsMapper;

    /**
     * 根据orderId 来查询订单详情
     * @param orderId
     * @return
     */
    @Override
    public OrderDetails selectOrderDetailsByOrderId(Long orderId) {

        OrderDetails orderDetails = orderDetailsMapper.selectOrderDetailsByOrderId(orderId);
        return orderDetails;
    }

    /**
     * 查询订单详细
     * 
     * @param id 订单详细主键
     * @return 订单详细
     */
    @Override
    public OrderDetails selectOrderDetailsById(Long id)
    {
        return orderDetailsMapper.selectOrderDetailsById(id);
    }

    /**
     * 查询订单详细列表
     * 
     * @param orderDetails 订单详细
     * @return 订单详细
     */
    @Override
    public List<OrderDetails> selectOrderDetailsList(OrderDetails orderDetails)
    {
        return orderDetailsMapper.selectOrderDetailsList(orderDetails);
    }

    /**
     * 新增订单详细
     * 
     * @param orderDetails 订单详细
     * @return 结果
     */
    @Override
    public int insertOrderDetails(OrderDetails orderDetails)
    {
        return orderDetailsMapper.insertOrderDetails(orderDetails);
    }

    /**
     * 修改订单详细
     * 
     * @param orderDetails 订单详细
     * @return 结果
     */
    @Override
    public int updateOrderDetails(OrderDetails orderDetails)
    {
        return orderDetailsMapper.updateOrderDetails(orderDetails);
    }

    /**
     * 批量删除订单详细
     * 
     * @param ids 需要删除的订单详细主键
     * @return 结果
     */
    @Override
    public int deleteOrderDetailsByIds(Long[] ids)
    {
        return orderDetailsMapper.deleteOrderDetailsByIds(ids);
    }

    /**
     * 删除订单详细信息
     * 
     * @param id 订单详细主键
     * @return 结果
     */
    @Override
    public int deleteOrderDetailsById(Long id)
    {
        return orderDetailsMapper.deleteOrderDetailsById(id);
    }
}
