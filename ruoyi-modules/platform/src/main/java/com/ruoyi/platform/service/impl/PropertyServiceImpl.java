package com.ruoyi.platform.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.platform.mapper.PropertyMapper;
import com.ruoyi.platform.domain.Property;
import com.ruoyi.platform.service.IPropertyService;

/**
 * 属性Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-11
 */
@Service
public class PropertyServiceImpl implements IPropertyService 
{
    @Autowired
    private PropertyMapper propertyMapper;

    /**
     * 查询属性
     * 
     * @param id 属性主键
     * @return 属性
     */
    @Override
    public Property selectPropertyById(Long id)
    {
        return propertyMapper.selectPropertyById(id);
    }

    /**
     * 查询属性列表
     * 
     * @param property 属性
     * @return 属性
     */
    @Override
    public List<Property> selectPropertyList(Property property)
    {
        return propertyMapper.selectPropertyList(property);
    }

    /**
     * 新增属性
     * 
     * @param property 属性
     * @return 结果
     */
    @Override
    public int insertProperty(Property property)
    {
        property.setCreateTime(DateUtils.getNowDate());
        property.setId(IdWorker.nextId());
        return propertyMapper.insertProperty(property);
    }

    /**
     * 修改属性
     * 
     * @param property 属性
     * @return 结果
     */
    @Override
    public int updateProperty(Property property)
    {
        property.setUpdateTime(DateUtils.getNowDate());
        return propertyMapper.updateProperty(property);
    }

    /**
     * 批量删除属性
     * 
     * @param ids 需要删除的属性主键
     * @return 结果
     */
    @Override
    public int deletePropertyByIds(Long[] ids)
    {
        return propertyMapper.deletePropertyByIds(ids);
    }

    /**
     * 删除属性信息
     * 
     * @param id 属性主键
     * @return 结果
     */
    @Override
    public int deletePropertyById(Long id)
    {
        return propertyMapper.deletePropertyById(id);
    }
}
