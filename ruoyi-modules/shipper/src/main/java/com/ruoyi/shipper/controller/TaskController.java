package com.ruoyi.shipper.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.shipper.domain.dto.TaskCarDto;
import com.ruoyi.shipper.domain.dto.TaskDto;
import com.ruoyi.shipper.domain.dto.TaskQuery;
import com.ruoyi.shipper.domain.vo.TaskVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.shipper.domain.Task;
import com.ruoyi.shipper.service.ITaskService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * taskController
 * 
 * @author wyh
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/task")
public class TaskController extends BaseController
{
    @Autowired
    private ITaskService taskService;

    /**
     * 条件查询
     */
//    @RequiresPermissions("task:task:select")
    @GetMapping("/select")
    public TableDataInfo select(@RequestBody TaskQuery taskQuery){
        startPage();
        List<TaskCarDto> list = taskService.selectList(taskQuery);
        return getDataTable(list);
    }

    /**
     * 查询状态
     */
    @GetMapping("/selectTaskStatus/{id}")
    public AjaxResult selectTaskStatus(@PathVariable("id")Long id){
        return success(taskService.selectTaskStatus(id));
    }

    /**
     * 获取task详细信息
     */
    @RequiresPermissions("task:task:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(taskService.selectTaskById(id));
    }

    /**
     * 新增task
     */
    @RequiresPermissions("task:task:add")
    @Log(title = "task", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TaskVO taskVO)
    {
        return toAjax(taskService.insertTask(taskVO));
    }

    /**
     * 修改状态
     * @param id
     * @return
     */
    @PutMapping("/upstate/{id}")
    public Long upstate(@PathVariable("id") Long id){

        return taskService.upstate(id);
    }
    @PutMapping("/upstateTransport/{id}")
    public Long upstateTransport(@PathVariable("id") Long id){

        return taskService.upstateTransport(id);
    }
    @PutMapping("/upstateComplete/{id}")
    public Long upstateComplete(@PathVariable("id") Long id){

        return taskService.upstateComplete(id);
    }

    /**
     * 根据用户id查询自己发布的任务
     */
    @GetMapping("/selectTaskByUserId")
    public TableDataInfo selectTaskByUserId(Long userId){

//         userId = SecurityUtils.getUserId();
         startPage();
         List<TaskDto> taskDtoList=taskService.selectTaskByUserId(userId);
        return getDataTable(taskDtoList);
    }

    /**
     * 查询task列表
     */
    @RequiresPermissions("task:task:list")
    @GetMapping("/list")
    public TableDataInfo list(Task task)
    {
        startPage();
        List<Task> list = taskService.selectTaskList(task);
        return getDataTable(list);
    }
    /**
     * 修改task
     */
    @RequiresPermissions("task:task:edit")
    @Log(title = "task", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Task task)
    {
        return toAjax(taskService.updateTask(task));
    }

    /**
     * 删除task
     */
    @RequiresPermissions("task:task:remove")
    @Log(title = "task", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(taskService.deleteTaskByIds(ids));
    }

    /**
 * 导出task列表
 */
@RequiresPermissions("task:task:export")
@Log(title = "task", businessType = BusinessType.EXPORT)
@PostMapping("/export")
public void export(HttpServletResponse response, Task task)
{
    List<Task> list = taskService.selectTaskList(task);
    ExcelUtil<Task> util = new ExcelUtil<Task>(Task.class);
    util.exportExcel(response, list, "task数据");
}
}
