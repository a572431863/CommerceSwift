package com.ruoyi.shipper.mapper;

import java.util.List;
import com.ruoyi.shipper.domain.Shipper;

/**
 * shipperMapper接口
 * 
 * @author wyh
 * @date 2024-03-06
 */
public interface ShipperMapper
{
    /**
     * 查询shipper
     * 
     * @param id shipper主键
     * @return shipper
     */
    public Shipper selectShsShipperById(Long id);

    /**
     * 查询shipper列表
     * 
     * @param shipper shipper
     * @return shipper集合
     */
    public List<Shipper> selectShsShipperList(Shipper shipper);

    /**
     * 新增shipper
     * 
     * @param shipper shipper
     * @return 结果
     */
    public int insertShsShipper(Shipper shipper);

    /**
     * 修改shipper
     * 
     * @param shipper shipper
     * @return 结果
     */
    public int updateShsShipper(Shipper shipper);

    /**
     * 删除shipper
     * 
     * @param id shipper主键
     * @return 结果
     */
    public int deleteShsShipperById(Long id);

    /**
     * 批量删除shipper
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteShsShipperByIds(Long[] ids);
}
