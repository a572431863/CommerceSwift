package com.ruoyi.shipper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * shipper对象 shs_shipper
 * 
 * @author wyh
 * @date 2024-03-06
 */
public class Shipper extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 货主姓名 */
    @Excel(name = "货主姓名")
    private String shipperName;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contactPerson;

    /** 货主电话号码 */
    @Excel(name = "货主电话号码")
    private String tel;

    /** 货主邮件 */
    @Excel(name = "货主邮件")
    private String email;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setShipperName(String shipperName) 
    {
        this.shipperName = shipperName;
    }

    public String getShipperName() 
    {
        return shipperName;
    }
    public void setContactPerson(String contactPerson) 
    {
        this.contactPerson = contactPerson;
    }

    public String getContactPerson() 
    {
        return contactPerson;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("shipperName", getShipperName())
            .append("contactPerson", getContactPerson())
            .append("tel", getTel())
            .append("email", getEmail())
            .append("address", getAddress())
            .toString();
    }
}
