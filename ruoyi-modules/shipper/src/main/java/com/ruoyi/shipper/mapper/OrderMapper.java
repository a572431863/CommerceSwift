package com.ruoyi.shipper.mapper;

import java.util.List;
import com.ruoyi.shipper.domain.Order;
import org.apache.ibatis.annotations.Update;

/**
 * orderMapper接口
 * 
 * @author wyh
 * @date 2024-03-07
 */
public interface OrderMapper 
{
    /**
     * 查询order
     * 
     * @param id order主键
     * @return order
     */
    public Order selectOrderById(Long id);

    /**
     * 查询order列表
     * 
     * @param order order
     * @return order集合
     */
    public List<Order> selectOrderList(Order order);

    /**
     * 新增order
     * 
     * @param order order
     * @return 结果
     */
    public int insertOrder(Order order);

    /**
     * 修改order
     * 
     * @param order order
     * @return 结果
     */
    public int updateOrder(Order order);

    /**
     * 删除order
     * 
     * @param id order主键
     * @return 结果
     */
    public int deleteOrderById(Long id);

    /**
     * 批量删除order
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderByIds(Long[] ids);

    /**
     * 更新订单状态
     * @param id
     * @return
     */
    @Update("update shs_order set state='已付款'where id=#{id}")
    Long upShsStatePayment(Long id);

    @Update("update shs_order set shs_state = 1,state='待装货' where id=#{id}")
    Long upShsStateShipment(Long id);

    @Update("update shs_order set shs_state=2,state='运输中'where id=#{id}")
    Long upShsStateTransport(Long id);

    @Update("update shs_order set shs_state=4,state='已完成'where id=#{id}")
    Long upShsStateComplete(Long id);

    /**
     * 更新车辆状态
     * @param id
     * @return
     */
    @Update("update shs_order set car_state=1 where id=#{id}")
    Long upCarStatePayment(Long id);

    @Update("update shs_order set car_state=2 where id=#{id}")
    Long upCarStateTransport(Long id);

    @Update("update shs_order set car_state=4 where id=#{id}")
    Long upCarStateComplete(Long id);

    @Update("update shs_order set state='已付款' where id=#{id}")
    void orderPay(Long id);
}
