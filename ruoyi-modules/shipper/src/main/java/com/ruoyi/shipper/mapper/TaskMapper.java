package com.ruoyi.shipper.mapper;

import java.util.List;
import com.ruoyi.shipper.domain.Task;
import com.ruoyi.shipper.domain.dto.TaskCarDto;
import com.ruoyi.shipper.domain.dto.TaskDto;
import com.ruoyi.shipper.domain.dto.TaskQuery;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * taskMapper接口
 * 
 * @author wyh
 * @date 2024-03-07
 */
public interface TaskMapper 
{
    /**
     * 查询task
     * 
     * @param id task主键
     * @return task
     */
    public Task selectTaskById(Long id);

    /**
     * 查询task列表
     * 
     * @param task task
     * @return task集合
     */
    public List<Task> selectTaskList(Task task);

    /**
     * 新增task
     * 
     * @param task task
     * @return 结果
     */
    public int insertTask(Task task);

    /**
     * 修改task
     * 
     * @param task task
     * @return 结果
     */
    public int updateTask(Task task);

    /**
     * 删除task
     * 
     * @param id task主键
     * @return 结果
     */
    public int deleteTaskById(Long id);

    /**
     * 批量删除task
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTaskByIds(Long[] ids);

    /**
     * 查询车主发布任务列表
     * @param taskQuery
     * @return
     */
    List<TaskCarDto> selectList(@Param("q") TaskQuery taskQuery);

    @Update("update shs_task set state ='待装车'  where id = #{id}")
    Long upstate(Long id);

    @Update("update shs_task set state ='未接单'  where id = #{id}")
    Long upstateTransport(Long id);

    @Update("update shs_task set state ='已接单'  where id = #{id}")
    Long upstateComplete(Long id);

    @Select("select loading_place,destination,gmt_create,cargo_detail,money from shs_task where user_id=#{userId}")
    List<TaskDto> selectTaskByUserId(Long userId);

    @Select("select state from shs_task where id = #{id}")
    String selectTaskStatus(Long id);
}
