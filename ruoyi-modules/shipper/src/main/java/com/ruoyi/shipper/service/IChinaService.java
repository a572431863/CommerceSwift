package com.ruoyi.shipper.service;

import java.util.List;
import com.ruoyi.shipper.domain.China;

/**
 * chinaService接口
 * 
 * @author wyh
 * @date 2024-03-07
 */
public interface IChinaService 
{
    /**
     * 查询china
     * 
     * @param id china主键
     * @return china
     */
    public China selectChinaById(Long id);

    /**
     * 查询china列表
     * 
     * @param china china
     * @return china集合
     */
    public List<China> selectChinaList(China china);

    /**
     * 新增china
     * 
     * @param china china
     * @return 结果
     */
    public int insertChina(China china);

    /**
     * 修改china
     * 
     * @param china china
     * @return 结果
     */
    public int updateChina(China china);

    /**
     * 批量删除china
     * 
     * @param ids 需要删除的china主键集合
     * @return 结果
     */
    public int deleteChinaByIds(Long[] ids);

    /**
     * 删除china信息
     * 
     * @param id china主键
     * @return 结果
     */
    public int deleteChinaById(Long id);
}
