package com.ruoyi.shipper.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.shipper.domain.Order;
import com.ruoyi.shipper.service.IOrderService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * orderController
 * 
 * @author wyh
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/order")
public class OrderController extends BaseController
{
    @Autowired
    private IOrderService orderService;

    /**
     * 支付
     */
    @GetMapping ("/orderPay")
    private AjaxResult orderPay(Long id){
        orderService.orderPay(id);
        return AjaxResult.success("支付成功");
    }
    /**
     * 修改货主支付状态
     */
    @PutMapping("/upShsStatePayment/{id}")
    public Long upShsStatePayment(@PathVariable("id") Long id){

        return orderService.upShsStatePayment(id);
    }

    /**
     * 修改货主装货状态
     */
    @PutMapping("/upShsStateShipment/{id}")
    public Long upShsStateShipment(@PathVariable("id") Long id){

        return orderService.upShsStateShipment(id);
    }

    /**
     * 修改货主运输状态
     */
    @PutMapping("/upShsStateTransport/{id}")
    public Long upShsStateTransport(@PathVariable("id") Long id){

        return orderService.upShsStateTransport(id);
    }

    /**
     * 修改货主完成订单状态
     */
    @PutMapping("/upShsStateComplete/{id}")
    public Long upShsStateComplete(@PathVariable("id") Long id){

        return orderService.upShsStateComplete(id);
    }


    /**
     * 修改车主装货状态
     */
    @PutMapping("/upstate/{id}")
    public Long upCarStateShipment(@PathVariable("id") Long id){

        return orderService.upCarStatePayment(id);
    }

    /**
     * 修改车主运输状态
     */
    @PutMapping("/upCarStateTransport/{id}")
    public Long upCarStateTransport(@PathVariable("id") Long id){

        return orderService.upCarStateTransport(id);
    }

    /**
     * 修改车主完成订单状态
     */
    @PutMapping("/upCarStateComplete/{id}")
    public Long upCarStateComplete(@PathVariable("id") Long id){

        return orderService.upCarStateComplete(id);
    }


    /**
     * 查询order列表
     */
    @RequiresPermissions("shipper:order:list")
    @GetMapping("/list")
    public TableDataInfo list(Order order)
    {
        startPage();
        List<Order> list = orderService.selectOrderList(order);
        return getDataTable(list);
    }

    /**
     * 导出order列表
     */
    @RequiresPermissions("shipper:order:export")
    @Log(title = "order", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Order order)
    {
        List<Order> list = orderService.selectOrderList(order);
        ExcelUtil<Order> util = new ExcelUtil<Order>(Order.class);
        util.exportExcel(response, list, "order数据");
    }

    /**
     * 获取order详细信息
     */
    @RequiresPermissions("shipper:order:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(orderService.selectOrderById(id));
    }

    /**
     * 新增order
     */
    @RequiresPermissions("shipper:order:add")
    @Log(title = "order", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Order order)
    {
        return toAjax(orderService.insertOrder(order));
    }

    /**
     * 修改order
     */
    @RequiresPermissions("shipper:order:edit")
    @Log(title = "order", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Order order)
    {
        return toAjax(orderService.updateOrder(order));
    }

    /**
     * 删除order
     */
    @RequiresPermissions("shipper:order:remove")
    @Log(title = "order", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(orderService.deleteOrderByIds(ids));
    }
}
