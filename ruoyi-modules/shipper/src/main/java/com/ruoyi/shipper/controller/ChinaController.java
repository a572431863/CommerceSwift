package com.ruoyi.shipper.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.shipper.domain.China;
import com.ruoyi.shipper.service.IChinaService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * chinaController
 * 
 * @author wyh
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/china")
public class ChinaController extends BaseController
{
    @Autowired
    private IChinaService chinaService;

    /**
     * 查询china列表
     */
    @RequiresPermissions("shipper:china:list")
    @GetMapping("/list")
    public TableDataInfo list(China china)
    {
        startPage();
        List<China> list = chinaService.selectChinaList(china);
        return getDataTable(list);
    }

    /**
     * 导出china列表
     */
    @RequiresPermissions("shipper:china:export")
    @Log(title = "china", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, China china)
    {
        List<China> list = chinaService.selectChinaList(china);
        ExcelUtil<China> util = new ExcelUtil<China>(China.class);
        util.exportExcel(response, list, "china数据");
    }

    /**
     * 获取china详细信息
     */
    @RequiresPermissions("shipper:china:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(chinaService.selectChinaById(id));
    }

    /**
     * 新增china
     */
    @RequiresPermissions("shipper:china:add")
    @Log(title = "china", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody China china)
    {
        return toAjax(chinaService.insertChina(china));
    }

    /**
     * 修改china
     */
    @RequiresPermissions("shipper:china:edit")
    @Log(title = "china", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody China china)
    {
        return toAjax(chinaService.updateChina(china));
    }

    /**
     * 删除china
     */
    @RequiresPermissions("shipper:china:remove")
    @Log(title = "china", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(chinaService.deleteChinaByIds(ids));
    }
}
