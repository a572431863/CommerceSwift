package com.ruoyi.shipper.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.shipper.domain.Shipper;
import com.ruoyi.shipper.service.IShsShipperService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * shipperController
 * 
 * @author wyh
 * @date 2024-03-06
 */
@RestController
@RequestMapping("/shipper")
public class ShipperController extends BaseController
{
    @Autowired
    private IShsShipperService shsShipperService;

    /**
     * 查询shipper列表
     */
    @RequiresPermissions("shipper:shipper:list")
    @GetMapping("/list")
    public TableDataInfo list(Shipper shipper)
    {
        startPage();
        List<Shipper> list = shsShipperService.selectShsShipperList(shipper);
        return getDataTable(list);
    }

    /**
     * 导出shipper列表
     */
    @RequiresPermissions("shipper:shipper:export")
    @Log(title = "shipper", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Shipper shipper)
    {
        List<Shipper> list = shsShipperService.selectShsShipperList(shipper);
        ExcelUtil<Shipper> util = new ExcelUtil<Shipper>(Shipper.class);
        util.exportExcel(response, list, "shipper数据");
    }

    /**
     * 获取shipper详细信息
     */
    @RequiresPermissions("shipper:shipper:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(shsShipperService.selectShsShipperById(id));
    }

    /**
     * 新增shipper
     */
    @RequiresPermissions("shipper:shipper:add")
    @Log(title = "shipper", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Shipper shipper)
    {
        return toAjax(shsShipperService.insertShsShipper(shipper));
    }

    /**
     * 修改shipper
     */
    @RequiresPermissions("shipper:shipper:edit")
    @Log(title = "shipper", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Shipper shipper)
    {
        return toAjax(shsShipperService.updateShsShipper(shipper));
    }

    /**
     * 删除shipper
     */
    @RequiresPermissions("shipper:shipper:remove")
    @Log(title = "shipper", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(shsShipperService.deleteShsShipperByIds(ids));
    }
}
