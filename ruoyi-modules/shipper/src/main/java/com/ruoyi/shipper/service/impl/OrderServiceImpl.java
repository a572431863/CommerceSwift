package com.ruoyi.shipper.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.shipper.mapper.OrderMapper;
import com.ruoyi.shipper.domain.Order;
import com.ruoyi.shipper.service.IOrderService;

/**
 * orderService业务层处理
 * 
 * @author wyh
 * @date 2024-03-07
 */
@Service
public class OrderServiceImpl implements IOrderService
{
    @Autowired
    private OrderMapper orderMapper;

    /**
     * 查询order
     * 
     * @param id order主键
     * @return order
     */
    @Override
    public Order selectOrderById(Long id)
    {
        return orderMapper.selectOrderById(id);
    }

    /**
     * 查询order列表
     * 
     * @param order order
     * @return order
     */
    @Override
    public List<Order> selectOrderList(Order order)
    {
        return orderMapper.selectOrderList(order);
    }

    /**
     * 新增order
     * 
     * @param order order
     * @return 结果
     */
    @Override
    public int insertOrder(Order order)
    {
        return orderMapper.insertOrder(order);
    }

    /**
     * 修改order
     * 
     * @param order order
     * @return 结果
     */
    @Override
    public int updateOrder(Order order)
    {
        return orderMapper.updateOrder(order);
    }

    /**
     * 批量删除order
     * 
     * @param ids 需要删除的order主键
     * @return 结果
     */
    @Override
    public int deleteOrderByIds(Long[] ids)
    {
        return orderMapper.deleteOrderByIds(ids);
    }

    /**
     * 删除order信息
     * 
     * @param id order主键
     * @return 结果
     */
    @Override
    public int deleteOrderById(Long id)
    {
        return orderMapper.deleteOrderById(id);
    }

    /**
     * 更新货主订单状态
     *
     * @param id
     * @return
     */
    @Override
    public Long upShsStatePayment(Long id) {
        return orderMapper.upShsStatePayment(id);
    }

    @Override
    public Long upShsStateShipment(Long id) {
        return orderMapper.upShsStateShipment(id);
    }

    @Override
    public Long upShsStateTransport(Long id) {
        return orderMapper.upShsStateTransport(id);
    }

    @Override
    public Long upShsStateComplete(Long id) {
        return orderMapper.upShsStateComplete(id);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public Long upCarStatePayment(Long id) {
        return orderMapper.upCarStatePayment(id);
    }

    @Override
    public Long upCarStateTransport(Long id) {
        return orderMapper.upCarStateTransport(id);
    }

    @Override
    public Long upCarStateComplete(Long id) {
        return orderMapper.upCarStateComplete(id);
    }

    @Override
    public void orderPay(Long id) {
        orderMapper.orderPay(id);
    }


}
