package com.ruoyi.shipper.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 货主任务
 */
public class TaskDto {
    /** 装货地点 */
    @Excel(name = "装货地点")
    private String loadingPlace;
    /** 目的地 */
    @Excel(name = "目的地")
    private String destination;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;
    /** 货物详情
     */
    @Excel(name = "货物详情 ")
    private String cargoDetail;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal money;


    public TaskDto() {
    }

    public TaskDto(String loadingPlace, String destination, Date gmtCreate, String cargoDetail, BigDecimal money) {
        this.loadingPlace = loadingPlace;
        this.destination = destination;
        this.gmtCreate = gmtCreate;
        this.cargoDetail = cargoDetail;
        this.money = money;
    }

    /**
     * 获取
     * @return loadingPlace
     */
    public String getLoadingPlace() {
        return loadingPlace;
    }

    /**
     * 设置
     * @param loadingPlace
     */
    public void setLoadingPlace(String loadingPlace) {
        this.loadingPlace = loadingPlace;
    }

    /**
     * 获取
     * @return destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * 设置
     * @param destination
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * 获取
     * @return gmtCreate
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * 设置
     * @param gmtCreate
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * 获取
     * @return cargoDetail
     */
    public String getCargoDetail() {
        return cargoDetail;
    }

    /**
     * 设置
     * @param cargoDetail
     */
    public void setCargoDetail(String cargoDetail) {
        this.cargoDetail = cargoDetail;
    }

    /**
     * 获取
     * @return money
     */
    public BigDecimal getMoney() {
        return money;
    }

    /**
     * 设置
     * @param money
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String toString() {
        return "TaskDto{loadingPlace = " + loadingPlace + ", destination = " + destination + ", gmtCreate = " + gmtCreate + ", cargoDetail = " + cargoDetail + ", money = " + money + "}";
    }
}
