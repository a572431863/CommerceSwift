package com.ruoyi.shipper.service;

import java.util.List;
import com.ruoyi.shipper.domain.Task;
import com.ruoyi.shipper.domain.dto.TaskCarDto;
import com.ruoyi.shipper.domain.dto.TaskDto;
import com.ruoyi.shipper.domain.dto.TaskQuery;
import com.ruoyi.shipper.domain.vo.TaskVO;

/**
 * taskService接口
 * 
 * @author wyh
 * @date 2024-03-07
 */
public interface ITaskService 
{
    /**
     * 查询task
     * 
     * @param id task主键
     * @return task
     */
    public Task selectTaskById(Long id);

    /**
     * 查询task列表
     * 
     * @param task task
     * @return task集合
     */
    public List<Task> selectTaskList(Task task);

    /**
     * 新增task
     * 
     * @param
     * @return 结果
     */
    public int insertTask(TaskVO taskVO);

    /**
     * 修改task
     * 
     * @param task task
     * @return 结果
     */
    public int updateTask(Task task);

    /**
     * 批量删除task
     * 
     * @param ids 需要删除的task主键集合
     * @return 结果
     */
    public int deleteTaskByIds(Long[] ids);

    /**
     * 删除task信息
     * 
     * @param id task主键
     * @return 结果
     */
    public int deleteTaskById(Long id);

    List<TaskCarDto> selectList(TaskQuery taskQuery);

    Long upstate(Long id);

    Long upstateTransport(Long id);

    Long upstateComplete(Long id);

    List<TaskDto> selectTaskByUserId(Long userId);

    String selectTaskStatus(Long id);
}
