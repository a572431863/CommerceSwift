package com.ruoyi.shipper.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;

import java.util.Date;

/**
 * 车主路线任务信息
 */
public class TaskCarDto {
    /** 装货地点 */
    @Excel(name = "装货地点")
    private String loadingPlace;

    /** 目的地 */
    @Excel(name = "目的地")
    private String destination;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /** (车长表)的主键 */
    @Excel(name = "(车长表)的主键")
    private Long carSize;

    /** 车型 */
    @Excel(name = "车型")
    private Long carType;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String plateNum;


    public TaskCarDto() {
    }

    public TaskCarDto(String loadingPlace, String destination, Date gmtCreate, Long carSize, Long carType, String plateNum) {
        this.loadingPlace = loadingPlace;
        this.destination = destination;
        this.gmtCreate = gmtCreate;
        this.carSize = carSize;
        this.carType = carType;
        this.plateNum = plateNum;
    }

    /**
     * 获取
     * @return loadingPlace
     */
    public String getLoadingPlace() {
        return loadingPlace;
    }

    /**
     * 设置
     * @param loadingPlace
     */
    public void setLoadingPlace(String loadingPlace) {
        this.loadingPlace = loadingPlace;
    }

    /**
     * 获取
     * @return destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * 设置
     * @param destination
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * 获取
     * @return gmtCreate
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * 设置
     * @param gmtCreate
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * 获取
     * @return carSize
     */
    public Long getCarSize() {
        return carSize;
    }

    /**
     * 设置
     * @param carSize
     */
    public void setCarSize(Long carSize) {
        this.carSize = carSize;
    }

    /**
     * 获取
     * @return carType
     */
    public Long getCarType() {
        return carType;
    }

    /**
     * 设置
     * @param carType
     */
    public void setCarType(Long carType) {
        this.carType = carType;
    }

    /**
     * 获取
     * @return plateNum
     */
    public String getPlateNum() {
        return plateNum;
    }

    /**
     * 设置
     * @param plateNum
     */
    public void setPlateNum(String plateNum) {
        this.plateNum = plateNum;
    }

    public String toString() {
        return "TaskDto{loadingPlace = " + loadingPlace + ", destination = " + destination + ", gmtCreate = " + gmtCreate + ", carSize = " + carSize + ", carType = " + carType + ", plateNum = " + plateNum + "}";
    }
}
