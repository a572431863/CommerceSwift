package com.ruoyi.shipper.mapper;

import java.util.List;
import com.ruoyi.shipper.domain.Address;

/**
 * addressMapper接口
 * 
 * @author wyh
 * @date 2024-03-07
 */
public interface AddressMapper 
{
    /**
     * 查询address
     * 
     * @param id address主键
     * @return address
     */
    public Address selectAddressById(Long id);

    /**
     * 查询address列表
     * 
     * @param address address
     * @return address集合
     */
    public List<Address> selectAddressList(Address address);

    /**
     * 新增address
     * 
     * @param address address
     * @return 结果
     */
    public int insertAddress(Address address);

    /**
     * 修改address
     * 
     * @param address address
     * @return 结果
     */
    public int updateAddress(Address address);

    /**
     * 删除address
     * 
     * @param id address主键
     * @return 结果
     */
    public int deleteAddressById(Long id);

    /**
     * 批量删除address
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAddressByIds(Long[] ids);
}
