package com.ruoyi.shipper.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;

import java.math.BigDecimal;
import java.util.Date;

public class TaskVO {
    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Long userId;
    /**
     * 货主id
     */
    @Excel(name = "货主id")
    private Long cargoOwnerId;
    /**
     * 货主姓名
     */
    @Excel(name = "货主姓名")
    private String cargoOwnerName;
    /**
     * 货物名称
     */
    @Excel(name = "货物名称")
    private String cargoName;
    /**
     * 货物详情
     */
    @Excel(name = "货物详情 ")
    private String cargoDetail;
    /**
     * 期望装货时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "期望装货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expectedLoadingTime;
    /**
     * 期望到达时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "期望到达时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expectedApproachTime;
    /**
     * 装货地点
     */
    @Excel(name = "装货地点")
    private String loadingPlace;
    /**
     * 目的地
     */
    @Excel(name = "目的地")
    private String destination;
    /**
     * 任务状态
     */
    @Excel(name = "任务状态")
    private String state;
    /**
     * 价格
     */
    @Excel(name = "价格")
    private BigDecimal money;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;
    /**
     * 1被删除，0未被删除
     */
    @Excel(name = "1被删除，0未被删除")
    private String isDeleted;
    /**
     * 用车需求
     */
    @Excel(name = "用车需求")
    private String vehicleDemand;
    /**
     * 用车类型：1-整车、2-拼车
     */
    @Excel(name = "用车类型：1-整车、2-拼车")
    private String transportationType;
    /**
     * 支付方式
     */
    @Excel(name = "支付方式")
    private String paymentMethod;
    /**
     * 其他需求
     */
    @Excel(name = "其他需求")
    private String otherRequirements;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String notes;
    /**
     * (车长表)的主键
     */
    @Excel(name = "(车长表)的主键")
    private Long carSize;
    /**
     * 车型
     */
    @Excel(name = "车型")
    private Long carType;

    public TaskVO() {
    }

    public TaskVO(Long userId, Long cargoOwnerId, String cargoOwnerName, String cargoName, String cargoDetail, Date expectedLoadingTime, Date expectedApproachTime, String loadingPlace, String destination, String state, BigDecimal money, Date gmtCreate, Date gmtModified, String isDeleted, String vehicleDemand, String transportationType, String paymentMethod, String otherRequirements, String notes, Long carSize, Long carType) {
        this.userId = userId;
        this.cargoOwnerId = cargoOwnerId;
        this.cargoOwnerName = cargoOwnerName;
        this.cargoName = cargoName;
        this.cargoDetail = cargoDetail;
        this.expectedLoadingTime = expectedLoadingTime;
        this.expectedApproachTime = expectedApproachTime;
        this.loadingPlace = loadingPlace;
        this.destination = destination;
        this.state = state;
        this.money = money;
        this.gmtCreate = gmtCreate;
        this.gmtModified = gmtModified;
        this.isDeleted = isDeleted;
        this.vehicleDemand = vehicleDemand;
        this.transportationType = transportationType;
        this.paymentMethod = paymentMethod;
        this.otherRequirements = otherRequirements;
        this.notes = notes;
        this.carSize = carSize;
        this.carType = carType;
    }

    /**
     * 获取
     * @return userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取
     * @return cargoOwnerId
     */
    public Long getCargoOwnerId() {
        return cargoOwnerId;
    }

    /**
     * 设置
     * @param cargoOwnerId
     */
    public void setCargoOwnerId(Long cargoOwnerId) {
        this.cargoOwnerId = cargoOwnerId;
    }

    /**
     * 获取
     * @return cargoOwnerName
     */
    public String getCargoOwnerName() {
        return cargoOwnerName;
    }

    /**
     * 设置
     * @param cargoOwnerName
     */
    public void setCargoOwnerName(String cargoOwnerName) {
        this.cargoOwnerName = cargoOwnerName;
    }

    /**
     * 获取
     * @return cargoName
     */
    public String getCargoName() {
        return cargoName;
    }

    /**
     * 设置
     * @param cargoName
     */
    public void setCargoName(String cargoName) {
        this.cargoName = cargoName;
    }

    /**
     * 获取
     * @return cargoDetail
     */
    public String getCargoDetail() {
        return cargoDetail;
    }

    /**
     * 设置
     * @param cargoDetail
     */
    public void setCargoDetail(String cargoDetail) {
        this.cargoDetail = cargoDetail;
    }

    /**
     * 获取
     * @return expectedLoadingTime
     */
    public Date getExpectedLoadingTime() {
        return expectedLoadingTime;
    }

    /**
     * 设置
     * @param expectedLoadingTime
     */
    public void setExpectedLoadingTime(Date expectedLoadingTime) {
        this.expectedLoadingTime = expectedLoadingTime;
    }

    /**
     * 获取
     * @return expectedApproachTime
     */
    public Date getExpectedApproachTime() {
        return expectedApproachTime;
    }

    /**
     * 设置
     * @param expectedApproachTime
     */
    public void setExpectedApproachTime(Date expectedApproachTime) {
        this.expectedApproachTime = expectedApproachTime;
    }

    /**
     * 获取
     * @return loadingPlace
     */
    public String getLoadingPlace() {
        return loadingPlace;
    }

    /**
     * 设置
     * @param loadingPlace
     */
    public void setLoadingPlace(String loadingPlace) {
        this.loadingPlace = loadingPlace;
    }

    /**
     * 获取
     * @return destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * 设置
     * @param destination
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * 获取
     * @return state
     */
    public String getState() {
        return state;
    }

    /**
     * 设置
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * 获取
     * @return money
     */
    public BigDecimal getMoney() {
        return money;
    }

    /**
     * 设置
     * @param money
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    /**
     * 获取
     * @return gmtCreate
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * 设置
     * @param gmtCreate
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * 获取
     * @return gmtModified
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * 设置
     * @param gmtModified
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    /**
     * 获取
     * @return isDeleted
     */
    public String getIsDeleted() {
        return isDeleted;
    }

    /**
     * 设置
     * @param isDeleted
     */
    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 获取
     * @return vehicleDemand
     */
    public String getVehicleDemand() {
        return vehicleDemand;
    }

    /**
     * 设置
     * @param vehicleDemand
     */
    public void setVehicleDemand(String vehicleDemand) {
        this.vehicleDemand = vehicleDemand;
    }

    /**
     * 获取
     * @return transportationType
     */
    public String getTransportationType() {
        return transportationType;
    }

    /**
     * 设置
     * @param transportationType
     */
    public void setTransportationType(String transportationType) {
        this.transportationType = transportationType;
    }

    /**
     * 获取
     * @return paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * 设置
     * @param paymentMethod
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * 获取
     * @return otherRequirements
     */
    public String getOtherRequirements() {
        return otherRequirements;
    }

    /**
     * 设置
     * @param otherRequirements
     */
    public void setOtherRequirements(String otherRequirements) {
        this.otherRequirements = otherRequirements;
    }

    /**
     * 获取
     * @return notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * 设置
     * @param notes
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * 获取
     * @return carSize
     */
    public Long getCarSize() {
        return carSize;
    }

    /**
     * 设置
     * @param carSize
     */
    public void setCarSize(Long carSize) {
        this.carSize = carSize;
    }

    /**
     * 获取
     * @return carType
     */
    public Long getCarType() {
        return carType;
    }

    /**
     * 设置
     * @param carType
     */
    public void setCarType(Long carType) {
        this.carType = carType;
    }

    public String toString() {
        return "TaskVO{userId = " + userId + ", cargoOwnerId = " + cargoOwnerId + ", cargoOwnerName = " + cargoOwnerName + ", cargoName = " + cargoName + ", cargoDetail = " + cargoDetail + ", expectedLoadingTime = " + expectedLoadingTime + ", expectedApproachTime = " + expectedApproachTime + ", loadingPlace = " + loadingPlace + ", destination = " + destination + ", state = " + state + ", money = " + money + ", gmtCreate = " + gmtCreate + ", gmtModified = " + gmtModified + ", isDeleted = " + isDeleted + ", vehicleDemand = " + vehicleDemand + ", transportationType = " + transportationType + ", paymentMethod = " + paymentMethod + ", otherRequirements = " + otherRequirements + ", notes = " + notes + ", carSize = " + carSize + ", carType = " + carType + "}";
    }
}
