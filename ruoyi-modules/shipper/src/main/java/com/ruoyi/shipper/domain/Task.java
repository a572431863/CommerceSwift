package com.ruoyi.shipper.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * task对象 shs_task
 * 
 * @author wyh
 * @date 2024-03-07
 */
public class Task extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 货主id */
    @Excel(name = "货主id")
    private Long cargoOwnerId;

    /** 货主姓名 */
    @Excel(name = "货主姓名")
    private String cargoOwnerName;

    /** 货主电话号码 */
    @Excel(name = "货主电话号码")
    private String tel;

    /** 货物名称 */
    @Excel(name = "货物名称")
    private String cargoName;

    /** 货物详情
 */
    @Excel(name = "货物详情 ")
    private String cargoDetail;

    /** 货物图片 */
    @Excel(name = "货物图片")
    private String cargoImg;

    /** 期望装货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "期望装货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expectedLoadingTime;

    /** 期望到达时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "期望到达时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expectedApproachTime;

    /** 装货地点 */
    @Excel(name = "装货地点")
    private String loadingPlace;

    /** 目的地 */
    @Excel(name = "目的地")
    private String destination;

    /** 任务状态 */
    @Excel(name = "任务状态")
    private String state;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 简介 */
    @Excel(name = "简介")
    private String briefIntroduction;

    /** 赏金 */
    @Excel(name = "赏金")
    private BigDecimal 
bounty;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal money;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    /** 1被删除，0未被删除 */
    @Excel(name = "1被删除，0未被删除")
    private String isDeleted;

    /** 用车需求 */
    @Excel(name = "用车需求")
    private String vehicleDemand;

    /** 用车类型：1-整车、2-拼车 */
    @Excel(name = "用车类型：1-整车、2-拼车")
    private String transportationType;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String paymentMethod;

    /** 其他需求 */
    @Excel(name = "其他需求")
    private String otherRequirements;

    /** 备注 */
    @Excel(name = "备注")
    private String notes;

    /** (车长表)的主键 */
    @Excel(name = "(车长表)的主键")
    private Long carSize;

    /** 车型 */
    @Excel(name = "车型")
    private Long carType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setCargoOwnerId(Long cargoOwnerId) 
    {
        this.cargoOwnerId = cargoOwnerId;
    }

    public Long getCargoOwnerId() 
    {
        return cargoOwnerId;
    }
    public void setCargoOwnerName(String cargoOwnerName) 
    {
        this.cargoOwnerName = cargoOwnerName;
    }

    public String getCargoOwnerName() 
    {
        return cargoOwnerName;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setCargoName(String cargoName) 
    {
        this.cargoName = cargoName;
    }

    public String getCargoName() 
    {
        return cargoName;
    }
    public void setCargoDetail(String cargoDetail) 
    {
        this.cargoDetail = cargoDetail;
    }

    public String getCargoDetail() 
    {
        return cargoDetail;
    }
    public void setCargoImg(String cargoImg) 
    {
        this.cargoImg = cargoImg;
    }

    public String getCargoImg() 
    {
        return cargoImg;
    }
    public void setExpectedLoadingTime(Date expectedLoadingTime) 
    {
        this.expectedLoadingTime = expectedLoadingTime;
    }

    public Date getExpectedLoadingTime() 
    {
        return expectedLoadingTime;
    }
    public void setExpectedApproachTime(Date expectedApproachTime) 
    {
        this.expectedApproachTime = expectedApproachTime;
    }

    public Date getExpectedApproachTime() 
    {
        return expectedApproachTime;
    }
    public void setLoadingPlace(String loadingPlace) 
    {
        this.loadingPlace = loadingPlace;
    }

    public String getLoadingPlace() 
    {
        return loadingPlace;
    }
    public void setdestination(String destination)
    {
        this.destination = destination;
    }

    public String getdestination()
    {
        return destination;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setBriefIntroduction(String briefIntroduction) 
    {
        this.briefIntroduction = briefIntroduction;
    }

    public String getBriefIntroduction() 
    {
        return briefIntroduction;
    }
    public void setbounty(BigDecimal bounty)
    {
        this.bounty = bounty;
    }

    public BigDecimal getbounty()
    {
        return bounty;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setgmtCreate(Date gmtCreate)
    {
        this.gmtCreate = gmtCreate;
    }

    public Date getgmtCreate()
    {
        return gmtCreate;
    }
    public void setGmtModified(Date gmtModified) 
    {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified() 
    {
        return gmtModified;
    }
    public void setIsDeleted(String isDeleted) 
    {
        this.isDeleted = isDeleted;
    }

    public String getIsDeleted() 
    {
        return isDeleted;
    }
    public void setVehicleDemand(String vehicleDemand) 
    {
        this.vehicleDemand = vehicleDemand;
    }

    public String getVehicleDemand() 
    {
        return vehicleDemand;
    }
    public void setTransportationType(String transportationType) 
    {
        this.transportationType = transportationType;
    }

    public String getTransportationType() 
    {
        return transportationType;
    }
    public void setPaymentMethod(String paymentMethod) 
    {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() 
    {
        return paymentMethod;
    }
    public void setOtherRequirements(String otherRequirements) 
    {
        this.otherRequirements = otherRequirements;
    }

    public String getOtherRequirements() 
    {
        return otherRequirements;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }
    public void setCarSize(Long carSize) 
    {
        this.carSize = carSize;
    }

    public Long getCarSize() 
    {
        return carSize;
    }
    public void setCarType(Long carType) 
    {
        this.carType = carType;
    }

    public Long getCarType() 
    {
        return carType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("cargoOwnerId", getCargoOwnerId())
            .append("cargoOwnerName", getCargoOwnerName())
            .append("tel", getTel())
            .append("cargoName", getCargoName())
            .append("cargoDetail", getCargoDetail())
            .append("cargoImg", getCargoImg())
            .append("expectedLoadingTime", getExpectedLoadingTime())
            .append("expectedApproachTime", getExpectedApproachTime())
            .append("loadingPlace", getLoadingPlace())
            .append(" destination", getdestination())
            .append("state", getState())
            .append("title", getTitle())
            .append("briefIntroduction", getBriefIntroduction())
            .append(" bounty", getbounty())
            .append("money", getMoney())
            .append(" gmtCreate", getgmtCreate())
            .append("gmtModified", getGmtModified())
            .append("isDeleted", getIsDeleted())
            .append("vehicleDemand", getVehicleDemand())
            .append("transportationType", getTransportationType())
            .append("paymentMethod", getPaymentMethod())
            .append("otherRequirements", getOtherRequirements())
            .append("notes", getNotes())
            .append("carSize", getCarSize())
            .append("carType", getCarType())
            .toString();
    }
}
