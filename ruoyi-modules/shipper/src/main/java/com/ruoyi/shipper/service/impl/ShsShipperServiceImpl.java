package com.ruoyi.shipper.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.shipper.mapper.ShipperMapper;
import com.ruoyi.shipper.domain.Shipper;
import com.ruoyi.shipper.service.IShsShipperService;

/**
 * shipperService业务层处理
 * 
 * @author wyh
 * @date 2024-03-06
 */
@Service
public class ShsShipperServiceImpl implements IShsShipperService 
{
    @Autowired
    private ShipperMapper shipperMapper;

    /**
     * 查询shipper
     * 
     * @param id shipper主键
     * @return shipper
     */
    @Override
    public Shipper selectShsShipperById(Long id)
    {
        return shipperMapper.selectShsShipperById(id);
    }

    /**
     * 查询shipper列表
     * 
     * @param shipper shipper
     * @return shipper
     */
    @Override
    public List<Shipper> selectShsShipperList(Shipper shipper)
    {
        return shipperMapper.selectShsShipperList(shipper);
    }

    /**
     * 新增shipper
     * 
     * @param shipper shipper
     * @return 结果
     */
    @Override
    public int insertShsShipper(Shipper shipper)
    {
        return shipperMapper.insertShsShipper(shipper);
    }

    /**
     * 修改shipper
     * 
     * @param shipper shipper
     * @return 结果
     */
    @Override
    public int updateShsShipper(Shipper shipper)
    {
        return shipperMapper.updateShsShipper(shipper);
    }

    /**
     * 批量删除shipper
     * 
     * @param ids 需要删除的shipper主键
     * @return 结果
     */
    @Override
    public int deleteShsShipperByIds(Long[] ids)
    {
        return shipperMapper.deleteShsShipperByIds(ids);
    }

    /**
     * 删除shipper信息
     * 
     * @param id shipper主键
     * @return 结果
     */
    @Override
    public int deleteShsShipperById(Long id)
    {
        return shipperMapper.deleteShsShipperById(id);
    }
}
