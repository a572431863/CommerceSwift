package com.ruoyi.shipper.service.impl;

import java.util.Date;
import java.util.List;

import com.ruoyi.common.core.utils.IdWorker;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.shipper.domain.dto.TaskCarDto;
import com.ruoyi.shipper.domain.dto.TaskDto;
import com.ruoyi.shipper.domain.dto.TaskQuery;
import com.ruoyi.shipper.domain.vo.TaskVO;
import com.ruoyi.shipper.mapper.ShipperMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.shipper.mapper.TaskMapper;
import com.ruoyi.shipper.domain.Task;
import com.ruoyi.shipper.service.ITaskService;

/**
 * taskService业务层处理
 *
 * @author wyh
 * @date 2024-03-07
 */
@Service
public class TaskServiceImpl implements ITaskService {
    @Autowired
    private TaskMapper taskMapper;

    /**
     * 查询task
     *
     * @param id task主键
     * @return task
     */
    @Override
    public Task selectTaskById(Long id) {
        return taskMapper.selectTaskById(id);
    }

    /**
     * 查询task列表
     *
     * @param task task
     * @return task
     */
    @Override
    public List<Task> selectTaskList(Task task) {
        return taskMapper.selectTaskList(task);
    }

    /**
     * 新增task
     * @return 结果
     */
    @Override
    public int insertTask(TaskVO taskVO) {
        Task task = new Task();
        task.setId(IdWorker.nextId());
        task.setgmtCreate(new Date());
        task.setUserId(SecurityUtils.getUserId());
        task.setCargoOwnerId(SecurityUtils.getUserId());
        task.setCargoName(SecurityUtils.getUsername());
        task.setCargoName(taskVO.getCargoName());
        task.setCargoDetail(taskVO.getCargoDetail());
        task.setExpectedLoadingTime(taskVO.getExpectedLoadingTime());
        task.setExpectedApproachTime(taskVO.getExpectedApproachTime());
        task.setLoadingPlace(taskVO.getLoadingPlace());
        task.setdestination(taskVO.getDestination());
        task.setState("未接单");
        task.setMoney(taskVO.getMoney());
        task.setGmtModified(new Date());
        task.setTransportationType(taskVO.getTransportationType());
        task.setPaymentMethod(taskVO.getPaymentMethod());
        task.setOtherRequirements(task.getOtherRequirements());
        task.setNotes(taskVO.getNotes());
        task.setCarSize(taskVO.getCarSize());
        task.setCarType(taskVO.getCarType());
        return taskMapper.insertTask(task);
    }

    /**
     * 修改task
     *
     * @param task task
     * @return 结果
     */
    @Override
    public int updateTask(Task task) {
        return taskMapper.updateTask(task);
    }

    /**
     * 批量删除task
     *
     * @param ids 需要删除的task主键
     * @return 结果
     */
    @Override
    public int deleteTaskByIds(Long[] ids) {
        return taskMapper.deleteTaskByIds(ids);
    }

    /**
     * 删除task信息
     *
     * @param id task主键
     * @return 结果
     */
    @Override
    public int deleteTaskById(Long id) {
        return taskMapper.deleteTaskById(id);
    }

    @Override
    public List<TaskCarDto> selectList(TaskQuery taskQuery) {
        taskQuery.setState("未接单");
        return taskMapper.selectList(taskQuery);
    }

    @Override
    public Long upstate(Long id) {
        return taskMapper.upstate(id);
    }

    @Override
    public Long upstateTransport(Long id) {
        return taskMapper.upstateTransport(id);
    }

    @Override
    public Long upstateComplete(Long id) {
        return taskMapper.upstateComplete(id);
    }

    @Override
    public List<TaskDto> selectTaskByUserId(Long userId) {

        return taskMapper.selectTaskByUserId(userId);
    }

    @Override
    public String selectTaskStatus(Long id) {
        return taskMapper.selectTaskStatus(id);
    }
}
