package com.ruoyi.shipper.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.shipper.mapper.ChinaMapper;
import com.ruoyi.shipper.domain.China;
import com.ruoyi.shipper.service.IChinaService;

/**
 * chinaService业务层处理
 * 
 * @author wyh
 * @date 2024-03-07
 */
@Service
public class ChinaServiceImpl implements IChinaService 
{
    @Autowired
    private ChinaMapper chinaMapper;

    /**
     * 查询china
     * 
     * @param id china主键
     * @return china
     */
    @Override
    public China selectChinaById(Long id)
    {
        return chinaMapper.selectChinaById(id);
    }

    /**
     * 查询china列表
     * 
     * @param china china
     * @return china
     */
    @Override
    public List<China> selectChinaList(China china)
    {
        return chinaMapper.selectChinaList(china);
    }

    /**
     * 新增china
     * 
     * @param china china
     * @return 结果
     */
    @Override
    public int insertChina(China china)
    {
        return chinaMapper.insertChina(china);
    }

    /**
     * 修改china
     * 
     * @param china china
     * @return 结果
     */
    @Override
    public int updateChina(China china)
    {
        return chinaMapper.updateChina(china);
    }

    /**
     * 批量删除china
     * 
     * @param ids 需要删除的china主键
     * @return 结果
     */
    @Override
    public int deleteChinaByIds(Long[] ids)
    {
        return chinaMapper.deleteChinaByIds(ids);
    }

    /**
     * 删除china信息
     * 
     * @param id china主键
     * @return 结果
     */
    @Override
    public int deleteChinaById(Long id)
    {
        return chinaMapper.deleteChinaById(id);
    }
}
