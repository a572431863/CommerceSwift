package com.ruoyi.shipper.service;

import java.util.List;
import com.ruoyi.shipper.domain.Order;

/**
 * orderService接口
 * 
 * @author wyh
 * @date 2024-03-07
 */
public interface IOrderService 
{
    /**
     * 查询order
     * 
     * @param id order主键
     * @return order
     */
    public Order selectOrderById(Long id);

    /**
     * 查询order列表
     * 
     * @param order order
     * @return order集合
     */
    public List<Order> selectOrderList(Order order);

    /**
     * 新增order
     * 
     * @param order order
     * @return 结果
     */
    public int insertOrder(Order order);

    /**
     * 修改order
     * 
     * @param order order
     * @return 结果
     */
    public int updateOrder(Order order);

    /**
     * 批量删除order
     * 
     * @param ids 需要删除的order主键集合
     * @return 结果
     */
    public int deleteOrderByIds(Long[] ids);

    /**
     * 删除order信息
     * 
     * @param id order主键
     * @return 结果
     */
    public int deleteOrderById(Long id);

    /**
     * 更新货主订单状态
     * @param id
     * @return
     */
    Long upShsStatePayment(Long id);

    Long upShsStateShipment(Long id);

    Long upShsStateTransport(Long id);

    Long upShsStateComplete(Long id);

    /**
     *
     * @param id
     * @return
     */
    Long upCarStatePayment(Long id);

    Long upCarStateTransport(Long id);

    Long upCarStateComplete(Long id);

    void orderPay(Long id);
}
