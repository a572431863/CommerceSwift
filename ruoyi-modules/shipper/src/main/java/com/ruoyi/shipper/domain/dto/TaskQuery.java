package com.ruoyi.shipper.domain.dto;

/**
 * 查询条件封装
 */
public class TaskQuery {
    // 装货地
    private String loadingPlace;
    // 卸货地
    private String destination;
    // 运输类型
    private String transportationType;

    // 任务状态
    private String state;
    // 车辆大小
    private Long carSize;
    // 车辆类型
    private Long carType;


    public TaskQuery() {
    }

    public TaskQuery(String loadingPlace, String destination, String transportationType, String state, Long carSize, Long carType) {
        this.loadingPlace = loadingPlace;
        this.destination = destination;
        this.transportationType = transportationType;
        this.state = state;
        this.carSize = carSize;
        this.carType = carType;
    }

    /**
     * 获取
     * @return loadingPlace
     */
    public String getLoadingPlace() {
        return loadingPlace;
    }

    /**
     * 设置
     * @param loadingPlace
     */
    public void setLoadingPlace(String loadingPlace) {
        this.loadingPlace = loadingPlace;
    }

    /**
     * 获取
     * @return destination
     */
    public String getDestination() {
        return destination;
    }

    /**
     * 设置
     * @param destination
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     * 获取
     * @return transportationType
     */
    public String getTransportationType() {
        return transportationType;
    }

    /**
     * 设置
     * @param transportationType
     */
    public void setTransportationType(String transportationType) {
        this.transportationType = transportationType;
    }

    /**
     * 获取
     * @return state
     */
    public String getState() {
        return state;
    }

    /**
     * 设置
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * 获取
     * @return carSize
     */
    public Long getCarSize() {
        return carSize;
    }

    /**
     * 设置
     * @param carSize
     */
    public void setCarSize(Long carSize) {
        this.carSize = carSize;
    }

    /**
     * 获取
     * @return carType
     */
    public Long getCarType() {
        return carType;
    }

    /**
     * 设置
     * @param carType
     */
    public void setCarType(Long carType) {
        this.carType = carType;
    }

    public String toString() {
        return "TaskQuery{loadingPlace = " + loadingPlace + ", destination = " + destination + ", transportationType = " + transportationType + ", state = " + state + ", carSize = " + carSize + ", carType = " + carType + "}";
    }
}
