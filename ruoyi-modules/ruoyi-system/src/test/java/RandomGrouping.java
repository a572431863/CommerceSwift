import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomGrouping {
    public static void main(String[] args) {
        String[] people = {"梁", "张", "温", "王", "黄", "陈"};
        List<String> peopleList = new ArrayList<>();
        Collections.addAll(peopleList, people);

        List<List<String>> groups = generateRandomGroups(peopleList);

        System.out.println("Random Groups:");
        for (List<String> group : groups) {
            System.out.println(group);
        }
    }

    public static List<List<String>> generateRandomGroups(List<String> people) {
        List<List<String>> groups = new ArrayList<>();

        Collections.shuffle(people); // Shuffle the list to randomize grouping

        for (int i = 0; i < people.size(); i += 2) {
            List<String> group = new ArrayList<>();
            group.add(people.get(i));
            if (i + 1 < people.size()) {
                group.add(people.get(i + 1));
            }
            groups.add(group);
        }

        return groups;
    }
}