package com.ruoyi.order.config;

import cn.hutool.core.lang.Assert;
import com.ruoyi.order.domain.Order;
import com.ruoyi.order.service.IOrderService;
import com.ruoyi.system.api.taskapi.ShsOrderApi;
import com.ruoyi.system.api.taskapi.TaskOrderApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Set;

@Component
@EnableScheduling
public class DelayedMessageHandler {

    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private TaskOrderApi taskOrderApi;
    @Autowired
    private IOrderService orderService;

    @Scheduled(fixedDelay = 1000) // 定时任务，每秒执行一次
    public void processDelayedMessages() {
        long currentTime = System.currentTimeMillis();
        Set<String> messages = redisTemplate.opsForZSet().rangeByScore("delayed_queue", 0, currentTime);
        if (messages != null) {
            for (String message : messages) {
                // 处理消息
                handleMessage(message);
                // 从 Sorted Set 中移除已处理的消息
                redisTemplate.opsForZSet().remove("delayed_queue", message);
            }
        }
    }

    public void handleMessage(String message) {
        String[] split = message.split(",");

        Long taskId = 0L,orderId = 0L;
        taskId = Long.parseLong(split[1]);
        orderId = Long.parseLong(split[0]);
        //Long aLong = shsOrderApi.upCarStateComplete(Long.parseLong(message));

        //如果数据库状态没变就取消订单
        if (! orderService.findOrderState(orderId)){
            //取消订单
            orderService.cancellationOfOrder(orderId);
            taskOrderApi.upstateTransport(taskId);
        }


    }
}
