package com.ruoyi.order.controller;

import java.util.List;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.order.model.dto.OrderTaskDto;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.order.domain.Order;
import com.ruoyi.order.service.IOrderService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * orderController
 *
 * @author wyh
 * @date 2024-03-12
 */
@RestController
@RequestMapping("/order")
public class OrderController extends BaseController {
    @Autowired
    private IOrderService orderService;

    @Autowired
    private RedisService redisService;


    @GetMapping("/test")
    public String test(){

        String msg = orderService.test();
        return msg;
    }

    /**
     * 按状态查询订单
     */
    @GetMapping("/findOrderByState")
    public List<Order> findOrder(Long carId, String state){
        Order order = new Order();
        order.setCarId(carId);
        order.setState(state);

        return orderService.selectOrderList(order);

    }

    /**
     * 修改订单状态
     */

    @GetMapping("/updateByState")
    public Integer updateByState(Long orderId, String state){

        Order order = new Order();
        order.setId(orderId);
        order.setState(state);

        return orderService.updateOrder(order);
    }

    @PostMapping("/confirm")
    public  AjaxResult confirm(@RequestBody OrderTaskDto orderTaskDto){
        Long userId = SecurityUtils.getUserId();
        redisService.setCacheObject("order:confirm"+userId,orderTaskDto, 30L,TimeUnit.SECONDS);
        return success(orderTaskDto);
    }
    /**
     * 添加订单
     */
    @RequiresPermissions("car-order:order:addOrder")
    @PostMapping("/addOrder")
    public AjaxResult addOrder() {
        Long userId = SecurityUtils.getUserId();
        OrderTaskDto orderTaskDto = redisService.getCacheObject("order:confirm" + userId);
        if (orderTaskDto == null){
            return error("订单已过期,请重新确认后提交");
        }
        orderService.addOrderByTask(orderTaskDto);
        return success();
    }


    /**
     * 查询order列表
     */
    @RequiresPermissions("car-order:order:list")
    @GetMapping("/list")
    public TableDataInfo list(Order order) {
        startPage();
        List<Order> list = orderService.selectOrderList(order);
        return getDataTable(list);
    }

    /**
     * 导出order列表
     */
    @RequiresPermissions("car-order:order:export")
    @Log(title = "order", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Order order) {
        List<Order> list = orderService.selectOrderList(order);
        ExcelUtil<Order> util = new ExcelUtil<Order>(Order.class);
        util.exportExcel(response, list, "order数据");
    }

    /**
     * 获取order详细信息
     */
    @RequiresPermissions("car-order:order:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(orderService.selectOrderById(id));
    }

    /**
     * 新增order
     */
    @RequiresPermissions("car-order:order:add")
    @Log(title = "order", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Order order) {
        return toAjax(orderService.insertOrder(order));
    }

    /**
     * 修改order
     */
    @RequiresPermissions("car-order:order:edit")
    @Log(title = "order", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Order order) {
        return toAjax(orderService.updateOrder(order));
    }

    /**
     * 删除order
     */
    @RequiresPermissions("car-order:order:remove")
    @Log(title = "order", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(orderService.deleteOrderByIds(ids));
    }
}
