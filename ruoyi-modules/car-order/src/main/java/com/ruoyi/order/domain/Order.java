package com.ruoyi.order.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * order对象 shs_order
 *
 * @author wyh
 * @date 2024-03-12
 */
public class Order extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 订单id
     */
    private Long id;

    /**
     * 订单号码
     */
    @Excel(name = "订单号码")
    private Long orderNum;

    /**
     * 雇主id
     */
    @Excel(name = "雇主id")
    private Long shipperId;

    /**
     * 车主id
     */
    @Excel(name = "车主id")
    private Long carId;

    /**
     * 订单金额
     */
    @Excel(name = "订单金额")
    private BigDecimal orderAmount;

    /**
     * 订单生成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单生成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderCreattime;

    /**
     * 订单状态
     */
    @Excel(name = "订单状态")
    private String state;

    @Excel(name = "车主订单状态（0 待付款，1 待收货，2 已完成，3 已取消）")
    private Integer carState;

    @Excel(name = "货主订单状态（0 待付款，1 待收货，2 已完成，3 已取消）")
    private Integer shsState;

    /**
     * 订单完成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "订单完成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderFinishtime;

    public Integer getCarState() {
        return carState;
    }

    public void setCarState(Integer carState) {
        this.carState = carState;
    }

    public Integer getShsState() {
        return shsState;
    }

    public void setShsState(Integer shsState) {
        this.shsState = shsState;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setOrderNum(Long orderNum) {
        this.orderNum = orderNum;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public void setShipperId(Long shipperId) {
        this.shipperId = shipperId;
    }

    public Long getShipperId() {
        return shipperId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public Long getCarId() {
        return carId;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderCreattime(Date orderCreattime) {
        this.orderCreattime = orderCreattime;
    }

    public Date getOrderCreattime() {
        return orderCreattime;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setOrderFinishtime(Date orderFinishtime) {
        this.orderFinishtime = orderFinishtime;
    }

    public Date getOrderFinishtime() {
        return orderFinishtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("orderNum", getOrderNum())
                .append("shipperId", getShipperId())
                .append("carId", getCarId())
                .append("orderAmount", getOrderAmount())
                .append("orderCreattime", getOrderCreattime())
                .append("state", getState())
                .append("orderFinishtime", getOrderFinishtime())
                .append("carState", getCarState())
                .append("shsState", getShsState())
                .toString();
    }
}
