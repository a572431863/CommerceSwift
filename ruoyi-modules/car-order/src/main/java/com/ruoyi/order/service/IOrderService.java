package com.ruoyi.order.service;

import java.util.List;
import com.ruoyi.order.domain.Order;
import com.ruoyi.order.model.dto.OrderTaskDto;

/**
 * orderService接口
 * 
 * @author wyh
 * @date 2024-03-12
 */
public interface IOrderService 
{
    /**
     * 查询order
     * 
     * @param id order主键
     * @return order
     */
    public Order selectOrderById(Long id);

    /**
     * 查询order列表
     * 
     * @param order order
     * @return order集合
     */
    public List<Order> selectOrderList(Order order);

    /**
     * 新增order
     * 
     * @param order order
     * @return 结果
     */
    public int insertOrder(Order order);

    /**
     * 修改order
     * 
     * @param order order
     * @return 结果
     */
    public int updateOrder(Order order);

    /**
     * 批量删除order
     * 
     * @param ids 需要删除的order主键集合
     * @return 结果
     */
    public int deleteOrderByIds(Long[] ids);

    /**
     * 删除order信息
     * 
     * @param id order主键
     * @return 结果
     */
    public int deleteOrderById(Long id);

    void addOrderByTask(OrderTaskDto orderTaskDto);

    Boolean findOrderState(Long id);

    void cancellationOfOrder(Long id);

    String test();
}
