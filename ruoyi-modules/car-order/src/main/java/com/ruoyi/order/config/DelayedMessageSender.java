package com.ruoyi.order.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class DelayedMessageSender {

    @Autowired
    private StringRedisTemplate redisTemplate;

    public void sendMessage(String message, long delay) {
        redisTemplate.opsForZSet().add("delayed_queue", message, System.currentTimeMillis() + delay);
    }
}