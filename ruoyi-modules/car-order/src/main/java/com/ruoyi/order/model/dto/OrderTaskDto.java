package com.ruoyi.order.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderTaskDto {

    private Long id;

    /**
     * 车主Id
     */
    private Long carId;

    /**
     * 任务Id
     */
    private Long taskId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 货主id
     */
    private Long cargoOwnerId;

    /**
     * 货主姓名
     */
    private String cargoOwnerName;

    /**
     * 货主电话号码
     */
    private String tel;

    /**
     * 货物名称
     */
    @Excel(name = "货物名称")
    private String cargoName;

    /**
     * 货物详情
     */
    @Excel(name = "货物详情 ")
    private String cargoDetail;

    /**
     * 货物图片
     */
    @Excel(name = "货物图片")
    private String cargoImg;

    /**
     * 期望装货时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "期望装货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expectedLoadingTime;

    /**
     * 期望到达时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "期望到达时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expectedApproachTime;

    /**
     * 装货地点
     */
    private String loadingPlace;

    /**
     * 目的地
     */
    private String destination;

    /**
     * 任务状态
     */
    private String state;

    /**
     * 标题
     */
    private String title;

    /**
     * 简介
     */
    private String briefIntroduction;

    /**
     * 赏金
     */
    private BigDecimal bounty;

    /**
     * 价格
     */
    private BigDecimal money;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtCreate;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date gmtModified;

    /**
     * 1被删除，0未被删除
     */
    private String isDeleted;

    /**
     * 用车需求
     */
    private String vehicleDemand;

    /**
     * 用车类型：1-整车、2-拼车
     */
    private String transportationType;

    /**
     * 支付方式
     */
    private String paymentMethod;

    /**
     * 其他需求
     */
    private String otherRequirements;

    /**
     * 备注
     */
    private String notes;

    /**
     * (车长表)的主键
     */
    private Long carSize;

    /**
     * 车型
     */
    private Long carType;
}
