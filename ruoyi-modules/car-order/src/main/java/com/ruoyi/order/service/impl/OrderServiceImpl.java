package com.ruoyi.order.service.impl;

import java.util.Date;

import java.util.List;

import com.ruoyi.common.core.enums.impl.BusinessCode;
import com.ruoyi.common.core.exception.Assert;
import com.ruoyi.common.core.utils.IdWorker;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.order.config.DelayedMessageHandler;
import com.ruoyi.order.config.DelayedMessageSender;
import com.ruoyi.order.model.dto.OrderTaskDto;
import com.ruoyi.system.api.taskapi.TaskOrderApi;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.order.mapper.OrderMapper;
import com.ruoyi.order.domain.Order;
import com.ruoyi.order.service.IOrderService;
import org.springframework.transaction.annotation.Transactional;

/**
 * orderService业务层处理
 *
 * @author wyh
 * @date 2024-03-12
 */
@Service
public class OrderServiceImpl implements IOrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private TaskOrderApi taskOrderApi;
    @Autowired
    private DelayedMessageSender delayedMessageSender;
    @Autowired
    private RedissonClient redissonClient;


    /**
     * 取消订单
     */
    @Override
    public void cancellationOfOrder(Long id) {

        Order order = new Order();
        order.setId(id);
        order.setShsState(4);
        order.setCarState(4);
        order.setState("取消订单");
        orderMapper.updateOrder(order);
    }



    @Override
    public Boolean findOrderState(Long id) {

        Order order = orderMapper.selectOrderById(id);
        if (order.getCarState() == 1 && order.getShsState() == 1){
            return false;
        }
        return true;
    }



    @Override
    public String test() {
        AjaxResult ajaxResult = taskOrderApi.selectTaskStatus(1L);
        String msg = (String) ajaxResult.get("msg");

        boolean equals = msg.equals("");
        return msg;
    }


    @Transactional
    @Override
    public void addOrderByTask(OrderTaskDto orderTaskDto) {
        AjaxResult ajaxResult = taskOrderApi.selectTaskStatus(orderTaskDto.getTaskId());
        String msg = (String) ajaxResult.get("msg");
        //判断是否已被接单
        Assert.error(!msg.equals("未接单"),BusinessCode.THE_ORDER_HAS_BEEN_ACCEPTED);
        //获取锁
        RLock rLock = redissonClient.getLock("Lock_" + orderTaskDto.getTaskId());
        try {
            //加锁
            rLock.lock();
            //给order赋值
            Order order = new Order();
            order.setId(IdWorker.nextId());
            order.setOrderNum(IdWorker.nextId());
            order.setShipperId(orderTaskDto.getCargoOwnerId());
            order.setCarId(orderTaskDto.getCarId());
            order.setOrderAmount(orderTaskDto.getBounty().add(orderTaskDto.getMoney()));
            order.setOrderCreattime(new Date());
            order.setState("待确认");
            order.setCarState(1);
            order.setShsState(1);
            order.setOrderFinishtime(null);
            orderMapper.insertOrder(order);

            //修改任务状态
            taskOrderApi.upstate(orderTaskDto.getTaskId());
            //Long id = 1767474190211874816L;
            //发送延时消息，15分钟后取消订单
            delayedMessageSender.sendMessage(order.getId() + ","+ orderTaskDto.getTaskId(), 30000);
        }finally {
            //释放锁
            rLock.unlock();
        }

    }




    /**
     * 查询order
     *
     * @param id order主键
     * @return order
     */
    @Override
    public Order selectOrderById(Long id) {
        return orderMapper.selectOrderById(id);
    }

    /**
     * 查询order列表
     *
     * @param order order
     * @return order
     */
    @Override
    public List<Order> selectOrderList(Order order) {
        return orderMapper.selectOrderList(order);
    }

    /**
     * 新增order
     *
     * @param order order
     * @return 结果
     */
    @Override
    public int insertOrder(Order order) {
        return orderMapper.insertOrder(order);
    }

    /**
     * 修改order
     *
     * @param order order
     * @return 结果
     */
    @Override
    public int updateOrder(Order order) {
        return orderMapper.updateOrder(order);
    }

    /**
     * 批量删除order
     *
     * @param ids 需要删除的order主键
     * @return 结果
     */
    @Override
    public int deleteOrderByIds(Long[] ids) {
        return orderMapper.deleteOrderByIds(ids);
    }

    /**
     * 删除order信息
     *
     * @param id order主键
     * @return 结果
     */
    @Override
    public int deleteOrderById(Long id) {
        return orderMapper.deleteOrderById(id);
    }


}
