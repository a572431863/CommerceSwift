package com.ruoyi.common.model.platform.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {

    //订单编号
    private String orderNum;

    //订单状态
    private Integer state;

    //用户ID
    private Long userId;

    //商品名称
    private String productName;

    //收货人名
    private String username;

    //收货人号码
    private String phonenumber;

    //下单时间
    private Date paymentTime;




}
