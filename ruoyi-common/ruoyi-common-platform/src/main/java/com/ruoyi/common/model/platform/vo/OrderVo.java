package com.ruoyi.common.model.platform.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderVo {

    //订单状态
    private Long state;

    //订单编号
    private String orderNum;

    //订单数量
    private Long productConut;

    //总价
    private BigDecimal totalAmount;

    //商品ID
    private Long productId;

    //商品名称
    private String productName;

    //商品图片
    private String image;

    //收货人姓名
    private String username;



}
