package com.ruoyi.common.model.platform.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductVo {

    //商品编号
    private String productCode;

    //商品名称
    private String productName;

    //图片
    private String image;

    //商品服务
    private Long productService;

    //商品状态
    private Long state;

    //销量
    private Long buyCount;

    //浏览量
    private Long view;

    //价格
    private BigDecimal price;

    //重量
    private BigDecimal weight;
}
