package com.ruoyi.common.model.platform.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderAddDto {

    //省
    private String province;

    //市
    private String city;

    //区
    private String area;

    //详情
    private String detailAddress;

    //收货人名
    private String username;

    //收货人号码
    private String phonenumber;

    //订单数量
    private Long productConut;

    //总价
    private BigDecimal totalAmount;

    //单价
    private BigDecimal price;

    //商品Id
    private Long productId;

}
