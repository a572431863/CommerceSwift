package com.ruoyi.common.core.enums;

public interface IErrorCode {
    int getCode();
    String getMessage();
}
