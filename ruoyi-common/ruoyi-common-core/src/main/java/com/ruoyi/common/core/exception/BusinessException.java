package com.ruoyi.common.core.exception;

import com.ruoyi.common.core.enums.IErrorCode;

public class BusinessException extends RuntimeException {

    private IErrorCode errorCode;
    public IErrorCode getErrorCode() {
        return errorCode;
    }

    public BusinessException() {
    }

    public BusinessException(IErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}