package com.ruoyi.common.core.exception;


import com.ruoyi.common.core.enums.IErrorCode;

/**
 * 自定义的断言类
 */
public class Assert {

    public static void error(IErrorCode errorCode) {
        throw new BusinessException(errorCode);
    }

    public static void error(Object obj,IErrorCode errorCode) {
        if (obj == null) {
            throw new BusinessException(errorCode);
        }
    }

    public static void error(boolean flag,IErrorCode errorCode) {
        if (flag) {
            throw new BusinessException(errorCode);
        }
    }
}