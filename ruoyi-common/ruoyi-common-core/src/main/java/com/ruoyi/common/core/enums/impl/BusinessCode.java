package com.ruoyi.common.core.enums.impl;

import com.ruoyi.common.core.enums.IErrorCode;
import lombok.Getter;

/**
 * 公共返回结果业务编码
 */
@Getter
public enum BusinessCode implements IErrorCode {


    /**
     *  用户错误：20001-29999
     */
    USER_EXISTED(20001, "用户已存在"),
    USER_LOGIN_ERROR(20002, "登陆失败，账号或密码错误！"),
    USER_NOT_LOGGED_IN(2001, "用户未登录"),
    USER_ACCOUNT_FORBIDDEN(20003, "账号已被禁用"),
    USER_NOT_EXIST(20004, "用户不存在"),
    USER_ALREADY_BINDED(20005,"用户已开户"),
    USER_CARD_ALREADY_BINDED(20006,"银行卡已被绑定"),
    USER_VERIFY_CODE_ERROR(20007,"验证码错误"),
    PHONE_EXISTED(20009,"手机号码已存在"),


    /* 业务错误：10000-19999 */
    ADD_SUCCESS(10000,"新增成功！"),
    ADD_FAIL(10001,"新增失败！"),
    MODIFY_SUCCESS(11000,"修改成功！"),
    MODIFY_FAIL(11001,"修改失败！"),
    REMOVE_SUCCESS(12000,"删除成功！"),
    REMOVE_FAIL(12001,"删除失败！"),
    LOGIN_FAIL(13001,"登录失败！用户名或密码错误！"),
    SESSION_TIMEOUT(13002,"session超时，请重新登录！"),
    NO_AUTH(13003,"没有权限访问请求资源，请切换账户后重试！"),
    ASSIGN_SUCCESS(15000,"分配成功！"),
    ASSIGN_FAIL(15001,"分配失败！"),
    ORDER_SUCCESS(16000,"排序成功！"),
    ORDER_FAIL(16001,"生成订单失败！"),
    SECKILL_NOT_EXISTS(17000,"秒杀活动不存在"),
    SECKILL_NOT_START(17001,"秒杀活动未开始"),
    SECKILL_ALREADY_END(17003,"秒杀活动已经结束"),
    SECKILL_REPEATED_BUY(17004,"不能重复抢购"),
    THE_ORDER_HAS_BEEN_ACCEPTED(17006,"该订单已被接单"),
    SECKILL_PRODUCT_SOLD_OUT(17005,"商品已售罄"),

    BALANCE_NOT_ENOUGH(50001, "用户余额不足, 请充值"),
    LEND_MONEY_FAILED(50002, "放款失败"),
    BANK_STATUS_MODIFY_FAILED(50002, "放款失败"),

    TOKEN_IS_NULL(50003, "TOKEN为空"),
    TOKEN_IS_INVALID(50004, "TOKEN不合法"),
    TOKEN_IS_EXPIRED(50004, "TOKEN已过期");

    private int code;

    private String message;

    BusinessCode(int code, String message){
        this.code = code;
        this.message = message;
    }

}